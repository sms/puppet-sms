# synths build from source
class sms::synth
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'

  require put::debian
  require sms::audio
  #require sms::builddeps

  $pkgs = [
    'libfftw3-dev',
    'yoshimi',
    'zynaddsubfx-lv2',
    'drumgizmo',
    'avldrums.lv2'
  ]
  ensure_packages($pkgs, {ensure => 'installed'})


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'

  put::buildfromgit { 'distrho-ports':
    repo     => 'https://github.com/DISTRHO/DISTRHO-Ports.git',
    revision => 'master',
    commands => [ 
                  'meson build --buildtype release',
                  'ninja -C build',
                  'ninja -C build install'
                ]
  }

#todo: https://github.com/surge-synthesizer/releases-xt/releases/download/1.1.2/surge-xt-linux-x64-1.1.2.deb
}

