# extra untested obs plugins etc.
class sms::obsextra
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian
  require sms::audio
  require sms::ndi
  require sms::v4l2sink
  require put::nodejs
  require sms::pythonstuff
  require sms::etcdefault
  require sms::obs

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'


    include put::nodejs

  put::buildfromgit { 'obs-v4l2sink':
    repo     => 'https://github.com/CatxFish/obs-v4l2sink.git',
    commands => ['mkdir -p build', "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr", 'make -j $(nproc)', 'make install'],
    require => Put::Buildfromgit['obs-studio'],
  }
  -> file { '/usr/lib/obs-plugins/obs-v4l2sink.so':
    ensure => 'link',
    target => '/usr/lib/x86_64-linux-gnu/obs-plugins/obs-v4l2sink.so',
  }

  
# FIXME: forgot what this is, and doesn't build
#  put::buildfromgit { 'obs-dir-watch-media':
#    repo     => 'https://github.com/exeldro/obs-dir-watch-media.git',
#    cwd      => 'build',
#    #revision => '4.7.0',
#    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make -j $(nproc)', 'make install'] ,
#    require => Put::Buildfromgit['obs-studio'],
#  }

#FIXME: does not build 
#  put::buildfromgit { 'obs-rtspserver':
#    repo     => 'https://github.com/iamscottxu/obs-rtspserver.git',
##    cwd      => 'build',
#    #revision => '4.7.0',
#    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'] ,
#    require => Put::Buildfromgit['obs-studio'],
#  }

  put::buildfromgit { 'obs-vnc':
    repo     => 'https://github.com/norihiro/obs-vnc.git',
    cwd      => 'build',
    #revision => '4.7.0',
    revision => '3af8844d34e115ae9de36aacacc9b5a9e8970352',
    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make -j $(nproc)', 'make install'] ,
  require => Put::Buildfromgit['obs-studio'],
  }
  -> file { '/usr/lib/obs-plugins/obs-vnc.so':
    ensure => 'link',
    target => '/usr/lib/x86_64-linux-gnu/obs-plugins/obs-vnc.so',
  }



# FIXME: can't find obs-frontend-api.h

#  put::buildfromgit { 'obs-multi-rtmp':
#    repo     => 'https://github.com/sorayuki/obs-multi-rtmp.git',
#    cwd      => 'build',
#    #revision => '4.7.0',
#    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'],
#    require => Put::Buildfromgit['obs-studio'],
#  }

#FIXME: /usr/local/src/obs-studio/libobs/obs-internal.h:40:10: fatal error: caption/caption.h: No such file or directory

#  put::buildfromgit { 'obs-gphoto':
#    repo     => 'https://github.com/Atterratio/obs-gphoto.git',
#    cwd      => 'build',
#    #revision => '4.7.0',
#    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make', 'make install'] ,
#    require => Put::Buildfromgit['obs-studio'],
# }






#  put::buildfromgit { 'obs-facemask':
#    repo     => 'https://github.com/stream-labs/facemask-plugin.git',
#    cwd      => 'build',
#    revision => 'master',
#    commands => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DPATH_OBS_STUDIO=${srcdir}/obs-studio/ -DCMAKE_INSTALL_PREFIX=/opt ..", 'make', 'make install'],
##cmake -DPATH_OBS_STUDIO=/usr/local/src/obs-studio/  -DCMAKE_INSTALL_PREFIX=/opt -DPATH_DLIB=../thirdparty/dlib/ ..
##cmake -DPATH_OBS_STUDIO=/usr/local/src/obs-studio/  -DCMAKE_INSTALL_PREFIX=/opt -DPATH_DLIB=../thirdparty/dlib/ -DPATH_FREETYPE=../thirdparty/freetype2  ..
#    #debiandeps => ['libdlib-dev', 'libopenblas-dev', 'liblapack-dev', 'nvidia-cuda-dev'],
#  }

  put::buildfromgit { 'obs-tablet-remote':
    repo     => 'https://github.com/t2t2/obs-tablet-remote.git',
    revision => '386d6d865892d21bbb088c80ef01a759d7f89ed7',
    commands => ['npm install', 'npm run build -- --homepage /'],
    require => Put::Buildfromgit['obs-studio'],
  }

  put::buildfromgit { 'miditoobs':
    repo     => 'https://github.com/lebaston100/MIDItoOBS.git',
    revision => 'master',
    # chmod is because program hardcodes debug.log in folde and cannot write there
    commands => ['pip3 install -r requirements.txt', 'chmod 777 -R .'],
    require => Put::Buildfromgit['obs-studio'],
  }

  # put::userunit {'miditoobs-web':
  #   after    => ['obs-web'],
  #   cwd      => '',
  #   # command  => '',
  #   user     => 'user',
  #   wantedby => [ 'xsession_started.target' ],
  # }


  put::buildfromgit { 'obs-web':
    repo     => 'https://github.com/Niek/obs-web.git',
    revision => 'master',
    commands => ['npm i', 'npm run build'],
    require => Put::Buildfromgit['obs-studio'],
  }

  
  put::userunit {'obs-web':
    command => '/usr/bin/npm run dev',
    cwd     => '/usr/local/src/obs-web/',
    user    => 'user',
    after       => [ 'jack.service'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],

    #wants       => [ 'obs.service'],
    #partof      => [ 'xsession.target' ],
    #wantedby    => [ 'xsession_started.target' ],
  }

  firewall { '121 obs-web':
    dport  => '5000',
    proto  => 'tcp',
    action => 'accept',
  }

    #FIXME: install obs-tablet-remote somewhere, and not run it in development mode all the time
    put::userunit {'obs-tablet-remote':
      command => '/usr/bin/npm run dev',
      cwd     => '/usr/local/src/obs-tablet-remote/',
      user    => 'user',
      after       => [ 'jack.service'],
      partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],
    }

}

