# software build from source
class sms::obs
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian
  require sms::audio
  require sms::gitrepos
  require sms::v4l2sink
  #require put::nodejs
  require sms::pythonstuff
  require sms::etcdefault
  require sms::gstd
  #require sms::obsfromsrc

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'

  $pkgs = [
    'ffmpeg',
    'build-essential',
    'checkinstall',
    'cmake',
    'libasound2-dev',
    'libavcodec-dev',
    'libavdevice-dev',
    'libavfilter-dev',
    'libavformat-dev',
    'libavutil-dev',
    'libcurl4-openssl-dev',
    'libfdk-aac-dev',
    'libfontconfig-dev',
    'libfreetype-dev',
    'libgl1-mesa-dev',
    'libjack-jackd2-dev',
    'libjansson-dev',
    'libluajit-5.1-dev',
    'libpulse-dev',
    'libqt5x11extras5-dev',
    'libqt5multimedia5',
    'lua5.3',
    'libspeexdsp-dev',
    'libswresample-dev',
    'libswscale-dev',
    'libudev-dev',
    'libv4l-dev',
    'libvlc-dev',
    'libx11-dev',
    'libx264-dev',
    'libxcb-shm0-dev',
    'libxcb-xinerama0-dev',
    'libxcomposite-dev',
    'libxinerama-dev',
    'pkg-config',
    'python3-dev',
    'qtbase5-dev',
    'libqt5svg5-dev',
    'swig',
    'ninja-build',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-bad1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'libgstrtspserver-1.0-dev',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-ugly',
    'libqt5gstreamerutils-1.0-0',
    'gstreamer1.0-tools',
    'libass-dev',
    'libsdl2-dev',
    'libtool',
    'libva-dev',
    'libvdpau-dev',
    'libvorbis-dev',
    'libxcb1-dev',
    'libxcb-shm0-dev',
    'libxcb-xfixes0-dev',
    'pkg-config',
    'texinfo',
    'wget',
    'zlib1g-dev',
    'nasm',
    'libx264-dev',
    'libx265-dev',
    'libnuma-dev',
    'libvpx-dev',
    'libfdk-aac-dev',
    'libmp3lame-dev',
    'libopus-dev',
    'avahi-utils',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'gstreamer1.0-plugins-base',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-bad',
    'gstreamer1.0-plugins-ugly',
    'gstreamer1.0-libav',
    'libgstrtspserver-1.0-dev',
    'python3-pip',
    'intel-media-va-driver-non-free',
    'i965-va-driver-shaders',
    'libxcb-composite0-dev'
  ]

  include put::nodejs

  ensure_packages($pkgs, {ensure => 'installed'}) 

  firewall { '121 obs websocket for remote control':
    dport  => '4444',
    proto  => 'tcp',
    action => 'accept',
  }


  firewall { '123 ndi streams tcp':
    dport  => '49152-65535',
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '123 ndi streams udp':
    dport  => '49152-65535',
    proto  => 'udp',
    action => 'accept',
  }

  firewall { '124 obs tablet remote':
    dport  => '8081',
    proto  => 'tcp',
    action => 'accept',
  }

  exec { 'mkdir -p /home/user/.config/obs-studio/basic/profiles/broadcast/':
    creates => '/home/user/.config/obs-studio/basic/profiles/broadcast',
    group   => 'user',
    user    =>  'user',
  }

  -> file { '/home/user/.config/obs-studio/basic/profiles/broadcast/basic.ini':
    content => epp("${module_name}/basic.ini.epp"), 
  }
  -> file { '/home/user/.config/obs-studio/basic/profiles/broadcast/service.json':
    content => epp("${module_name}/service.json.epp"), 
  }
  -> file { '/home/user/.config/obs-studio/basic/profiles/broadcast/streamEncoder.json':
    content => epp("${module_name}/streamEncoder.json.epp"), 
  }


 
  -> put::userunit { 'obs':
      command     => '/usr/bin/obs',
      user        => 'user',
      ## check if it still works with autostart false
      autostart   => false,
      autorestart => true,
      after       => [ 'jack.service', 'ardour.service'],
      wants       => [ 'jack.service', 'pulseup.service'],
      partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],

      #partof      => [ 'xsession_started.target' ],
      #wantedby    => [ 'xsession_started.target' ],

    }



}

