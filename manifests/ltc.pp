# av sync test stuff
class sms::ltc (
  String  $jack_cmdline = '-d dummy',
  String $user = 'user',
  String $userdir = '/home/user',
  Boolean $netmaster = false,
  Boolean $special_routing = false,
  )
  {
  tag 'sms'


  # https://www.justaphase.blog/home/2020/4/23/daw-timecode-sync
  # ltc wave generator: https://elteesee.pehrhovey.net/
  # https://obsproject.com/forum/resources/rtc-timecode-generator.1301/
  # https://github.com/arikrupnik/ltcsync

  require put::debian

  $srcdir = '/usr/local/src'


  $pkgs = [
    'jack-tools',
    'jackd',
    'libltc-dev',
    'libsndfile1-dev',
    'build-essential',
    'autoconf',
    'pkg-config',
    'libtool',
    'gstreamer1.0-tools',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'libcairo2-dev',

    #for pam
    'libwxgtk3.2-dev',
    'portaudio19-dev',
    'libsndfile1-dev',
    'libsamplerate0-dev',
    'libavahi-client-dev',
    'libcap-dev',
    'libssl-dev',
    'libcurl4-openssl-dev',


  ]


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  ensure_packages($pkgs, {ensure => 'installed'})

  put::buildfromgit { 'ltc-tools':
    repo     => 'https://github.com/x42/ltc-tools.git',
    commands => [
      'make',
      'make install'
    ],
  }
 
  put::userunit { 'ltcgen':
    command  => '/usr/local/bin/jltcgen',
    user     => 'user',
    after    => [ 'jack.service' ],
    wants    => [ 'jack.service' ],
    wantedby => [ 'xsession.target' ],
  }


  put::buildfromgit { 'gst-avsynctestsrc':
    repo     => 'https://github.com/MaZderMind/gst-avsynctestsrc',
    commands => [
      './autogen.sh',
      'make',
      'make install'
    ],
  }

  put::buildfromgit { 'ltc-delay':
    repo     => 'https://github.com/x42/ltc-delay.git',
    commands => [
      'make',
      'make install'
    ],
  }


  # FIXME: needs paths specified on cmake, also needs to be tested first, seems very specific for rpi with touchscreen
  #put::buildfromgit { 'pam':
  #  repo     => 'https://github.com/martim01/pam.git',
  #  cwd      => 'build',
  #  commands => [
  #    'cmake -DDIR_BASE=/srv/pam_external ..',
  #    'cmake --build .',
  #    'cmake --build . --target install',
  #  ]
  #}

  # does not build
  #put::buildfromgit { 'ltcvideosplit':
  #  repo     => 'https://github.com/gisogrimm/ltcvideosplit.git',
  #  commands => [
  #    'make',
  #    'make install'
  #  ],
  #}
 

}
