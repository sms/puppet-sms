# sets up pipewire, instead of jack / pulse
class sms::pipewire (
  String $user = 'user',
  String $userdir = '/home/user',
)

{
  require sms::audio

  class {'sms::audioservices':
    pipewire =>  true,
  }

  $build_deps = [
    'autopoint', 
    'debhelper',
    'dh-autoreconf',
    'dh-strip-nondeterminism',
    'doxygen',
    'dwz',
    'gettext',
    'intltool-debian',
    'libarchive-zip-perl',
    'libbluetooth-dev',
    'libclang-cpp11',
    'libdebhelper-perl',
    'libfile-stripnondeterminism-perl',
    'libldacbt-abr-dev',
    'libldacbt-abr2',
    'libldacbt-enc-dev',
    'libldacbt-enc2',
    'libopenaptx-dev',
    'libopenaptx0',
    'libsbc-dev',
    'libsub-override-perl',
    'libsystemd-dev',
    'po-debconf',
    'xmltoman',
    'libdbus-1-dev',
    'gstreamer1.0-tools',
    'alsa-utils',
    'libasound2-dev',
    'libudev-dev',
    'cmake',
    'libsystemd-dev',
    'libsystemd0',
    'libcanberra-dev',
    'libcanberra0',
    'libreadline-dev',
    'libreadline8',
    'libsdl2-dev',
    'libcap-dev',
    'libcap2',
    'libldacbt-abr-dev',
    'libldacbt-abr2',
    'libldacbt-enc-dev',
    'libldacbt-enc2',
    'libfdk-aac2',
    'libfdk-aac-dev',
    'libavcodec-dev',
    'libavutil-dev',
    'libudev-dev',
    'libudev1',
  ]
  ensure_packages($build_deps, {ensure => 'present'})

  $remove_pkgs = [
    'libpipewire-0.3-0',
    'libpipewire-0.3-dev',
    'libpipewire-0.3-modules',
    'pipewire',
    'pipewire-audio-client-libraries',
    'pipewire-bin',
    'pipewire-doc',
    'pipewire-tests',
  ]
  ensure_packages($remove_pkgs, {ensure => 'absent'})



  $pulsedaemon = @("EOF")
daemonize = no
EOF

  file { "/etc/pulse/daemon.conf":
    content => $pulsedaemon,
  }

  $pulseclient = @("EOF")
autospawn = no
EOF

  file { "/etc/pulse/client.conf":
    content => $pulseclient,
  }

  file { "/etc/pulse/client.conf.d/01-enable-autospawn.conf":
    ensure => absent,
  }
  
  -> exec { 'apt-update for pipewire':
    command   => '/usr/bin/apt-get update',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
  }
  -> exec { 'apt-install meson':
    command   => '/usr/bin/apt-get install -y -t bullseye-backports meson ',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

  }
  -> put::buildfromgit { 'pipewire':
    revision => '0.3.69',
    repo      => 'https://gitlab.freedesktop.org/pipewire/pipewire.git',
    #revision => 'avb', 
    #repo      => 'https://gitlab.freedesktop.org/wtaymans/pipewire.git',


    commands  => ['./autogen.sh --prefix=/usr', 'make', 'make install', 'ldconfig'],
  } 
  -> put::buildfromgit { 'wireplumber':
    revision => '0.4.14', 
    repo      => 'https://github.com/PipeWire/wireplumber.git',
    commands  => ['meson setup --prefix=/usr build', 'meson compile -C build', 'ninja -C build install'],
  } 
  #    -> put::buildfromgit { 'helvum':
  #  revision => 'main', 
  #  repo     => 'https://gitlab.freedesktop.org/pipewire/helvum.git',
  #  #cwd      =>  './build',
  #  commands => [
  #    'meson setup build', 
  #    'cd build ; meson compile',
  #    'cd build ; meson install'
  #  ],
  #} 
 
  -> file { '/etc/pipewire/pipewire.conf':
    content => epp("${module_name}/pipewire.conf.epp"), 
  }

  -> file { '/etc/pipewire/jack.conf':
    content => epp("${module_name}/jack.conf.epp"), 
  }

  -> file { '/etc/pipewire/pipewire-pulse.conf':
    content => epp("${module_name}/pipewire-pulse.conf.epp"), 
  }

  -> file { '/etc/pipewire/client-rt.conf':
    content => epp("${module_name}/client-rt.conf.epp"), 
  }

  -> file { '/etc/pipewire/client.conf':
    content => epp("${module_name}/client.conf.epp"), 
  }

  # -> exec { '/usr/sbin/setcap cap_net_raw,cap_net_admin+ep /usr/bin/pipewire-avb': }
  -> exec { '/usr/sbin/setcap cap_net_raw,cap_net_admin+ep /usr/bin/pipewire': }
} 
