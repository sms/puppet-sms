# avb
class sms::avb (
  )
  {
  
  $pkgs = [
    'linux-headers-amd64',
    'build-essential',
    'meson', 
    'flex',
    'bison',
    'glib2.0',
    'libcmocka-dev',
    'autoconf',
    'libtool',
    'autopoint',
    'libncurses-dev',
    'gstreamer1.0-vaapi',
    'gstreamer1.0-tools',
  ]

  ensure_packages($pkgs, { ensure => 'installed' })

  exec { '/usr/bin/apt-get build-dep -y gstreamer1.0-plugins-bad': }

  exec { '/usr/sbin/setcap cap_net_raw,cap_net_admin+ep /usr/bin/gst-launch-1.0': }

  -> put::buildfromgit { 'libavtp':
    revision => 'master',
    repo      => 'https://github.com/Avnu/libavtp.git',
    commands  => [
      'meson build',
      'ninja -C build',
      'ninja -C build install',
      'ldconfig'
    ]
  } 

  -> put::buildfromgit { 'gst-plugins-bad':
    revision => '1.18',
    repo      => 'https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad.git',
    commands  => [
      'meson build --prefix=/usr/local',
      'ninja -C build',
      'ninja -C build install'
    ]
  } 
  -> put::buildfromgit { 'alsa-lib':
    revision => 'v1.2.2',
    repo      => 'https://github.com/alsa-project/alsa-lib.git',
    commands  => [
      'autoreconf -i',
      './configure --prefix=/usr/local',
      'make',
      'make install'
    ]
  } 
  -> put::buildfromgit { 'alsa-utils':
    revision => 'v1.2.2',
    repo      => 'https://github.com/alsa-project/alsa-utils.git',
    commands  => [
      'autoreconf -i',
      './configure --prefix=/usr/local',
      'make',
      'make install'
    ]
  } 



  -> put::buildfromgit { 'alsa-plugins':
    revision => 'v1.2.2',
    repo      => 'https://github.com/alsa-project/alsa-plugins.git',
    commands  => [
      'autoreconf -i',
      './configure --prefix=/usr/local',
      'make',
      'make install'
    ]
  } 

  -> put::buildfromgit { 'avdecc-lib':
    revision => 'master',
    repo      => 'https://github.com/audioscience/avdecc-lib.git',
    commands  => [
      'cmake .',
    ]
  } 

  -> put::buildfromgit { 'avdecc':
    revision => 'main',
    repo      => 'https://github.com/L-Acoustics/avdecc.git',
    commands  => [
      './gen_cmake.sh -release',
      'cmake .',
      'make'

    ]
  } 

  -> put::buildfromgit { 'hive':
    revision => 'main',
    repo      => 'https://github.com/christophe-calmejane/Hive.git',
    commands  => [
      'sleep 1',

    ]
  } 



  file { '/etc/alsa/conf.d/100-avb.conf':
    content => epp("${module_name}/100-avb.conf.epp"), 
  }



 
}
