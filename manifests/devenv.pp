# some utils to help development

class sms::devenv

{



  $kpkgs = [
    'fzf',
    'ripgrep',
    'fd-find',
    'ranger',
    'tmux',
    'mosh',
    'screen',
    'vim',
    'emacs',
    'jekyll',
    'locate'
  ]

  ensure_packages($kpkgs, {ensure => 'installed'})
}
