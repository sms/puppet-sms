# splash mapper
class sms::splashmapper (
  String $user = 'user',
  String $userdir = '/home/user',
) {
  require put::debian
  #require sms::shmdata
  require sms::flatpak

  $pkgs = [
  #calimiro
  'git-lfs',
  'meshlab',
  'libopencv-dev',
  'libopencv-contrib-dev',
  'libgsl-dev',
  'libglm-dev',
  'libjsoncpp-dev',
  'libeigen3-dev',
  'graphviz',
  'libx11-dev',
  'libxrandr-dev',
  'libxinerama-dev',
  'libxcursor-dev',
  'libxi-dev',
  'mesa-common-dev',
  'libgl1-mesa-dev',
  'libglu1-mesa-dev',
  'libxxf86vm-dev',
  'libgphoto2-dev',


  # splashmapper
  'doxygen',
  'build-essential',
  'git-core',
  'cmake',
  'cmake-extras',
  'libxrandr-dev',
  'libxi-dev',
  'mesa-common-dev',
  'libgsl0-dev',
  'libatlas3-base',
  'libgphoto2-dev',
  'libz-dev',
  'libxinerama-dev',
  'libxcursor-dev',
  'python3-dev',
  'yasm',
  'portaudio19-dev',
  'python3-numpy',
  'libopencv-dev',
  'libjsoncpp-dev',
  'libavcodec-dev',
  'libavformat-dev',
  'libavutil-dev',
  'libswscale-dev',
  'ninja-build',
  'libwayland-dev',
  'libxkbcommon-dev',
  'libzmq3-dev',

  #openmvg
  'libceres-dev',

  ]

  ensure_packages($pkgs, {ensure => 'installed'})

  Exec { '/usr/bin/flatpak install --system --or-update -y --noninteractive com.gitlab.sat_metalab.Splash': }

  # 
  # FIXME: source build is not working 

  # put::buildfromgit { 'calimiro':
  #  repo     => 'https://gitlab.com/splashmapper/calimiro',
  #  revision => '0.2.1',
  #  cwd      => 'build',
  #  commands => [
  #    '../setup.sh',
  #    'cmake ..',
  #    'make -j 8 ',
  #    'make install',
  #  ],
  #}


  #put::buildfromgit { 'splashmapper':
  #  repo     => 'https://gitlab.com/splashmapper/splash',
  #  revision => '0.10.6',
  #  cwd      => 'build',
  #  commands => [
  #    'cmake -DUSE_SYSTEM_LIBS=OFF -DBUILD_GENERIC_ARCH=OFF ..',
  #    'ninja',
  #    'ninja install',
  #  ],
  #}
 
}
