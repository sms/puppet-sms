# git repos
class sms::gitrepos (
  String $user = 'user',
  String $userdir = '/home/user',
  String $gitensure = 'latest',
  Boolean $localchanges = false,
  Boolean $enable_obs = true,
  Boolean $enable_ardour = true,
  Boolean $enable_website = true,

) {


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  git::config { 'user.name':
    value => 'John Doe',
  }

  git::config { 'user.email':
    value => "john.doe@${facts['networking']['hostname']}",
  }

  $wt = lookup('sms::glwritetoken', String, 'first', '')

  unless $wt == '' {
    notice("We have write access for sms repo")
    $sms_src = {
      'origin' => 'https://git.puscii.nl/sms/streaming-media-stuff.git',
      'write'  => "https://oauth2:${wt}@git.puscii.nl/sms/streaming-media-stuff.git",
    }
  } else {
    notice("No write access for sms repo")

    $sms_src = {
        'origin' => 'https://git.puscii.nl/sms/streaming-media-stuff.git',
     }
  }

  vcsrepo { '/home/user/src/streaming-media-stuff':
    ensure   => $gitensure,
    provider => git,
    owner    => $user,
    group    => $user,
    remote   => 'origin',
    revision => 'internet',
    user     => $user,
    keep_local_changes => $localchanges,
    source   => $sms_src,
    submodules => false,
  }

 if $enable_ardour == true {
    unless $wt == '' {
      notice("We have write access for ardour repo")
      $ardour_src = {
        'origin' => 'https://git.puscii.nl/sms/ardour.git',
        'write'  => "https://oauth2:${wt}@git.puscii.nl/sms/ardour.git",
      }
    } else {
      notice("No write access for ardour repo")

      $ardour_src = {
          'origin' => 'https://git.puscii.nl/sms/ardour.git',
       }
    }
   vcsrepo { "${userdir}/media/ardour":
      ensure   => $gitensure,
      provider => git,
      owner    => $user,
      group    => $user, 
      revision => 'master',
      remote   => 'origin',
      keep_local_changes => $localchanges,
      user     => $user,
      source   => $ardour_src
    }
  }


  if $enable_obs == true {
    unless $wt == '' {
      notice("We have write access for obs repo")
      $obs_src = {
        'origin' => 'https://git.puscii.nl/sms/obs.git',
        'write'  => "https://oauth2:${wt}@git.puscii.nl/sms/obs.git",
      }
    } else {
      notice("No write access for obs repo")

      $obs_src = {
          'origin' => 'https://git.puscii.nl/sms/obs.git',
       }
    }

 
     vcsrepo { "/home/${user}/.config/obs-studio":
        ensure   => $gitensure,
        provider => git,
        owner    => $user,
        group    => $user,
        revision => 'internet',
        remote   => 'origin',
        user     => $user,
        keep_local_changes => $localchanges,
        source   => $obs_src    
     }
  }

  if $enable_website == true {
    unless $wt == '' {
      notice("We have write access for web repo")
      $web_src = {
        'origin' => 'https://git.puscii.nl/admradio/pap-website-jekyll.git',
        'write'  => "https://oauth2:${wt}@git.puscii.nl/admradio/pap-website-jekyll.git",
      }
    } else {
      notice("No write access for web repo")

      $web_src = {
          'origin' => 'https://git.puscii.nl/admradio/pap-website-jekyll.git',
       }
    }
 
    vcsrepo { '/home/user/src/leftover-website':
      ensure   => $gitensure,
      provider => git,
      owner    => $user,
      group    => $user,
      remote   => 'origin',
      user     => $user,
      keep_local_changes => $localchanges,
      source   => $web_src,
    }
  }

}

