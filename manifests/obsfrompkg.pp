# obs packages
class sms::obsfrompkg
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian
  require sms::audio
  require sms::builddeps
  require sms::ndi
  require sms::v4l2sink
  require put::nodejs
  require sms::pythonstuff

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'


  exec{ 'purge stuff we install from source':
    command => '/usr/bin/apt-get -y purge obs-studio'
  }




  $pkgs = [
    'ffmpeg',
    'build-essential',
    'checkinstall',
    'cmake',
    'libasound2-dev',
    'libavcodec-dev',
    'libavdevice-dev',
    'libavfilter-dev',
    'libavformat-dev',
    'libavutil-dev',
    'libcurl4-openssl-dev',
    'libfdk-aac-dev',
    'libfontconfig-dev',
    'libfreetype-dev',
    'libgl1-mesa-dev',
    'libjack-jackd2-dev',
    'libjansson-dev',
    'libluajit-5.1-dev',
    'libpulse-dev',
    'libqt5x11extras5-dev',
    'libqt5multimedia5',
    'lua5.3',
    'libspeexdsp-dev',
    'libswresample-dev',
    'libswscale-dev',
    'libudev-dev',
    'libv4l-dev',
    'libvlc-dev',
    'libx11-dev',
    'libx264-dev',
    'libxcb-shm0-dev',
    'libxcb-xinerama0-dev',
    'libxcomposite-dev',
    'libxinerama-dev',
    'pkg-config',
    'python3-dev',
    'qtbase5-dev',
    'libqt5svg5-dev',
    'swig',
    'ninja-build',
    'meson',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-bad1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'libgstrtspserver-1.0-dev',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-ugly',
    'libqt5gstreamerutils-1.0-0',
    'gstreamer1.0-tools',
    'libass-dev',
    'libsdl2-dev',
    'libtool',
    'libva-dev',
    'libvdpau-dev',
    'libvorbis-dev',
    'libxcb1-dev',
    'libxcb-shm0-dev',
    'libxcb-xfixes0-dev',
    'pkg-config',
    'texinfo',
    'wget',
    'zlib1g-dev',
    'nasm',
    'libx264-dev',
    'libx265-dev',
    'libnuma-dev',
    'libvpx-dev',
    'libfdk-aac-dev',
    'libmp3lame-dev',
    'libopus-dev',
    'avahi-utils',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'gstreamer1.0-plugins-base',
    'gstreamer1.0-plugins-good',
    'gstreamer1.0-plugins-bad',
    'gstreamer1.0-plugins-ugly',
    'gstreamer1.0-libav',
    'libgstrtspserver-1.0-dev',
    'python3-pip',
    'intel-media-va-driver-non-free',
    'i965-va-driver-shaders',
    'libxcb-composite0-dev'
  ]

  include put::nodejs

  ensure_packages($pkgs, {ensure => 'installed'}) 

  
  put::buildfromgit { 'obs-studio':
    repo_name                                            => 'obs-studio',
    tag                                                  => ['obsbuild'],
    # websocket does not compile with this one# revision => '27.2.0',
    #revision                                            => '27.1.3', # < this works with websocket for sure
    revision                                             =>  '27.2.4',
    #srcdir                                              => "/usr/local/src",
    repo                                                 => 'https://github.com/obsproject/obs-studio.git',
    cwd                                                  => './build',
    commands                                             => [
      'cmake -DUNIX_STRUCTURE=1 -DBUILD_BROWSER=OFF -DENABLE_PIPEWIRE=OFF -DCMAKE_INSTALL_PREFIX=/usr ..', 
      # for master / newer version # 'cmake -DBUILD_BROWSER=ON -DENABLE_PIPEWIRE=OFF -DCEF_ROOT_DIR=/opt/cef -DENABLE_AJA=OFF  -DCMAKE_INSTALL_PREFIX=/usr ..'
      'make -j $(nproc)', 
      'make install', 
      'ldconfig'],
  } 

  put::buildfromgit { 'obs-gstreamer':
    repo     => 'https://github.com/fzwoch/obs-gstreamer.git',
    revision => '6b15a17e4691b754ee442964e5c97c9faef15343',
    commands => ['mkdir -p build', 'meson --buildtype=release --prefix /usr build', 'ninja -C build', 'ninja -C build install'],
    require => Put::Buildfromgit['obs-studio'],
  }
  -> file { '/usr/lib/obs-plugins/obs-gstreamer.so':
    ensure => 'link',
    target => '/usr/lib/x86_64-linux-gnu/obs-plugins/obs-gstreamer.so',
  }


  put::buildfromgit { 'obs-websocket':
    repo      => 'https://github.com/obsproject/obs-websocket.git',
    cwd       => 'build',
    #revision => '4.9.1',
    revision  => 'df2049b7515ff47a33193865c8ee04ef3bb0ce9b',
    #revision  =>  '4.9.1-compat',
    commands  => [ "cmake -DLIBOBS_INCLUDE_DIR=${srcdir}/obs-studio/libobs -DCMAKE_INSTALL_PREFIX=/usr ..", 'make -j $(nproc) ', 'make install'],
   require => Put::Buildfromgit['obs-studio'],
  }
  -> file { '/usr/lib/obs-plugins/obs-websocket-compat.so':
    ensure => 'link',
    target => '/usr/lib/x86_64-linux-gnu/obs-plugins/obs-websocket-compat.so',
  }



}

