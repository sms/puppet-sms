# feedmanager
class sms::feedmanager
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian
  require sms::audio
  require sms::gitrepos
  require sms::v4l2sink
  #require put::nodejs
  require sms::pythonstuff
  require sms::etcdefault
  require sms::gstd
  #require sms::obsfromsrc

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'

  put::userunit {'feedmanager':
      command     => '/home/user/src/streaming-media-stuff/feedmanager/feeds.py',
      cwd         => '/home/user/src/streaming-media-stuff/feedmanager',
      user        => 'user',
      autorestart =>  true,
      after       => [ 'obs.service'],
      partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],
    }



}

