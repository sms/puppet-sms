# patched parole that outputs to jack and obs
class sms::parole
(
)

{
    put::buildfromgit { 'parole':
    revision => 'master',
    repo      => 'https://git.puscii.nl/sms/parole.git',
    commands  => [
      './autogen.sh',
      'make',
    ],
  } 

  -> put::userunit {'parole-player1':
    command => '/usr/local/src/parole/src/parole',
    cwd     => '/home/user/',
    user    => 'user',
    after       => [ 'pulseup.service'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }



}
 
