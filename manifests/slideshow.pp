# displays slideshow fullscreen
class sms::slideshow (
  String $slidedir = '/home/user/slides',
  )
  {

  require put::debian
  require put::smsprep
  require put::xfdesktop
  require sms::etcdefault

  file { "${slidedir}":
    ensure => directory,
    owner  => 'user',
  }


  $srcdir = '/root/src'


  $pkgs = [
    'impressive',
    'pdf-presenter-console',
    'mpv',
    'eog',
    'mc',
  ]


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  ensure_packages($pkgs, {ensure => 'installed'})

  put::userunit { 'slideshow':
    #command      => "/usr/bin/impressive --scale -t WipeRight --poll 60 --wrap --cachefile ${slidedir}/slideshow.cache --auto 10 ${slidedir}",
    command      => "/usr/bin/mpv --loop-playlist=inf --fullscreen --image-display-duration=5 ${slidedir}", 
    user         => 'user',
    autorestart  => true,
    after        => [ 'pulseaudio.service' ],
    partof       => [ 'xsession.target' ],
    wantedby     => [ 'xsession.target' ],
  }

}

