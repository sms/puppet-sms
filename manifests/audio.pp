# shared audio stuff
class sms::audio (
  String  $jack_cmdline = '-d dummy',
  String $user = 'user',
  String $userdir = '/home/user',
  Boolean $netmaster = false,
  Boolean $special_routing = false,
  )
  {
  tag 'sms'

  require put::debian
  require sms::kernel
  require sms::pythonstuff
  require put::smsprep
  require sms::etcdefault
  
  #TODO: echo -n performance | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor

  $srcdir = '/root/src'


  $pkgs = [
    'aj-snapshot',
    'jack-tools',
    'jackd',
    'moc',
    'libltc-dev'
  ]


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  ensure_packages($pkgs, {ensure => 'installed'})


  #FIXME: this is because of a bug in liquidsoap (crashes when loading 1 of the plugins)
  package{ 'frei0r-plugins':
    ensure => absent,
  }


  #do not start pasystray, it makes messages appear all the time
  file { '/etc/xdg/autostart/pasystray.desktop':
    ensure => absent,
  }

  file { '/home/user/.config/mpv':
    ensure => 'directory'
  }
  file { '/home/user/.config/mpv/mpv.conf':
    content => epp("${module_name}/mpv.conf.epp"),

  }

  $vlcrc = @("EOF")
[core]
aout=jack

[jack] # JACK audio output
jack-auto-connect=1
jack-connect-regex=ardour:VLC
jack-name=vlc
jack-gain=1.000000


EOF

  file { "/home/${user}/.config/vlc":
    ensure => directory,
  }

  file { "/home/${user}/.config/vlc/vlcrc":
    content => $vlcrc,
  }




  $mocp_conf = @("EOF")
MusicDir = /home/user/
StartInMusicDir = yes
ShowStreamErrors = yes
Repeat = no
Shuffle = no
AutoNext = yes
InputBuffer = 512                  # Minimum value is 32KB
OutputBuffer = 512                 # Minimum value is 128KB
Prebuffering = 64
SoundDriver JACK
JackClientName = "moc"
JackStartServer = no
JackOutLeft = "ardour:MOC/audio_in 1"
JackOutRight = "ardour:MOC/audio_in 2"
Softmixer_SaveState = no 
Equalizer_SaveState = no
ShowTime = IfAvailable
Precache = yes
SavePlaylist = yes
SyncPlaylist = yes
SeekTime = 1
SilentSeekTime = 15
UseRealtimePriority = no
EOF



  $limits_conf = @("EOF")

@audio   -  rtprio     95
@audio   -  memlock    unlimited
@audio   -  nice      -19

EOF

  file { '/etc/security/limits.d/audio.conf':
    content => $limits_conf,
  }

  file { '/etc/jack-plumbing.conf':
    content => epp("${module_name}/jack_plumbing.conf.epp"),
    notify  => Put::Userunit['jack-plumbing'],

  }

  put::userunit { 'jack-plumbing':
    command      => '/usr/bin/jack-plumbing -o /etc/jack-plumbing.conf',
    user         => 'user',
    execstartpre => ['jack_lsp'],
    autorestart  => true,
    after        => [ 'jack.service' ],
    wants        => [ 'jack.service' ],
    partof       => [ 'xsession.target' ],
    wantedby     => [ 'xsession.target' ],
  }

  put::userunit { 'mocp':
    command  => '/usr/bin/mocp -F',
    user     => 'user',
    after    => [ 'jack.service', 'ardour.service' ],
    wants    => [ 'jack.service', 'ardour.service' ],
    wantedby => [ 'xsession.target' ],
  }


}

#autostart => true,
#partof    => [ 'xsession.target' ],
