# rust (debian versin is old)
class sms::rust
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{

  package { 'rustc':
    ensure => 'absent'
  }
  package { 'cargo':
    ensure => 'absent'
  }



  file { '/usr/local/bin/rustup-init':
    source => 'https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init',
    mode   => '0755'
  }

  exec { '/usr/local/bin/rustup-init -y':
  }

}
