# ros
class sms::ros {
  require put::debian


  exec { 'import ros gpg key (is not on keyserver)':
    command => 'wget -O- https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

  }


  #apt::key { 'Open whispersystems':
  #  id     => 'DBA36B5181D0C816F630E889D980A17457F6FB06',
  #  server => 'pgp.mit.edu',
 # } ->
    -> apt::source { 'ros_apt':
    ensure   => present,
    comment  => 'ros',
    location => 'http://packages.ros.org/ros/ubuntu',
    release  => 'buster',
    repos    => 'main',
    include  => {
      'src' => false,
      'deb' => true,
    },
  }

  -> exec { 'apt-update for ros':
    command => '/usr/bin/apt-get update',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',

  }

  -> exec { 'apt install for ros':
    command   => '/usr/bin/apt install -y signal-desktop',
    logoutput => 'on_failure',
    path      => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
    require   => Exec['apt-update'],

  }


}
