# for scanning books
class sms::bookscanner(
  $desktop = true,
  ){

#include nodejs

#FIXME: install those if its a graphical desktop
  if ($desktop) {

    $pkgsgui = [
      'gtkam',
      'xchat',
      'ocrmypdf',
      'ocrfeeder',
      'gscan2pdf',
      'gocr',
      'xsane',
      'calibre',
    ]

    ensure_packages($pkgsgui, {ensure => 'latest'})


  }
  $pkgs = [ 'screen',
    'cadaver',
    'tesseract-ocr-nld',
    'pdfsandwich',
    'pandoc',
    'sane',
    'tesseract-ocr-eng',
    'texlive-extra-utils',
    'texlive-full',
    'imagemagick',
    'gphoto2',
    'thunar-vcs-plugin',
    'gvfs-backends',
    'cuneiform',
    'unpaper',
    'ocrad',
    'lios',
    'gcc-arm-none-eabi',
    'subversion',
    'make',
    'libusb-dev',
    'liblua5.3-dev',
    'liblua50-dev',
    'liblua5.2-dev',
    'liblua5.1-0-dev',
    'libreadline-dev',
    'python2.7-dev',
    'python-pip',
    'build-essential',
    'pkg-config',
    'libffi-dev',
    'libturbojpeg-dev',
    'libmagickwand-dev',
    'python-cffi',
    #not in buster#    'python-pyside',
    'python-qtpy',
    'djvubind',
    'libgphoto2-dev',
    'cmake',
    'libqt4-dev',
    'python-gphoto2cffi',
    'python3-gphoto2cffi',
    'python-hidapi',
    'ruby',
    'ruby-dev',
    #node module stupid (redeclares Package[npm]# 'npm',
    'libsass-dev',
    'libpangocairo-1.0',
    'libxml2',
    'libblas3',
    'liblapack3',
    'python3-dev',
    'python3-pip'
  ]

ensure_packages($pkgs, {ensure => 'latest'})

#sudo pip install lupa --install-option="--no-luajit"

  $py2pkgs = [
    'lupa',
#     'gphoto2-cffi',
    'jpegtran-cffi',
    'Flask',
    'requests',
    'zipstream',
    'tornado',
    'Wand',
    'pyside',
    'chdkptp.py',
    'hocr-tools',
    #server down#  'http://buildbot.diybookscanner.org/nightly/spreads-latest.tar.gz',
#     'spreads'

  ]
  ensure_packages( $py2pkgs,
  {
    ensure   => present,
    provider => 'pip',
  })

  $py3pkgs = [
    'kraken'
  ]

  ensure_packages( $py3pkgs,
  {
    ensure   => present,
    provider => 'pip3',
  })


  $rubypkgs = [
    'pdfbeads',
  ]
  ensure_packages( $rubypkgs,
  {
    ensure   => present,
    provider => 'gem',
  })

  $nodepkgs = [
    'node-sass',
    'sass',
  ]
  ensure_packages( $nodepkgs,
  {
    ensure   => latest,
    provider => 'npm',
    install_options => [ '--unsafe-perm', '-g' ],
  })




  $srcdir = '/root/src'

  Exec {
    user    => 'root',
    cwd     => "${srcdir}/rtl_433/build",
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  exec { 'install lupa':
    command => 'pip install lupa --install-option="--no-luajit"',
    cwd     => '/root',
  }


# does not work, use images
#  vcsrepo { "${srcdir}/chdk":
#    ensure   => present,
#    provider => svn,
#    source   => 'https://tools.assembla.com/svn/chdk/trunk',
#  }
#  -> exec { 'chdk make ':
#    command => 'make PLATFORM=ixus160_elph160 PLATFORMSUB=100a OPT_USE_GCC_EABI=1 fir',
#    cwd     => "${srcdir}/chdk",
#  }

  vcsrepo { "${srcdir}/chdkptp":
    ensure   => present,
    provider => svn,
    source   => 'https://tools.assembla.com/svn/chdkptp/trunk',
  }
  -> exec { 'chdkptp config ':
    command => 'cp config-sample-linux.mk config.mk',
    cwd     => "${srcdir}/chdkptp",
  }
  -> exec { 'chdkptp make ':
    command => 'make',
    cwd     => "${srcdir}/chdkptp",
  }

### http://tools.assembla.com/chdkde/browser/trunk/tools/ptpcam
# builds: http://forum.chdk-treff.de/viewtopic.php?f=7&t=2122

# only on windows: https://chdk.fandom.com/wiki/PtpCamGui

# https://chdk.fandom.com/wiki/PTP_Extension

  vcsrepo { "${srcdir}/piscanner":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/aarobc/piscanner.git',
  }

# FIXME: make this build (there is a patch in bookscanner.git)
  vcsrepo { "${srcdir}/spreads":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/DIYBookScanner/spreads.git',
  }

  vcsrepo { "${srcdir}/ocr-gt-tools":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/UB-Mannheim/ocr-gt-tools.git',
  }

# make -f debian.mk apt-get
# enable cgi in apache
# make -f apache.mk APACHE_DIR="/var/www/html" deploy

  vcsrepo { "${srcdir}/hocr-proofreader":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/not-implemented/hocr-proofreader.git',
  }




#

}

