# ndi simple monitor
class sms::nsm
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{

  file { '/opt/nsm':
    ensure => 'directory',
  }

  -> archive { '/usr/local/src/nsm.tar.gz':
    ensure        => present,
    extract       => true,
    extract_path  => '/opt/nsm/',
    source        => 'https://github.com/keijiro/Nsm/releases/download/2.0.0/Nsm-Linux-2.0.0.tgz',
    creates       => '/opt/nsm/Nsm.x86_64',
    cleanup       => true,
  }

  -> file { '/usr/local/bin/nsm':
    ensure => 'link',
    target => "/opt/nsm/Nsm.x86_64",
  }
}
