# python packages needed by various other things
class sms::pythonstuff
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  $pkgs = [
    'python3-pip',
    'python3-rtmidi',
  ]

  ensure_packages($pkgs, {ensure => 'installed'}) 


  #FIXME: not sure if this is the best way to do that
  file { '/usr/lib/python3.11/EXTERNALLY-MANAGED':
    ensure =>  'absent'
  }
  file { '/usr/lib/python3.12/EXTERNALLY-MANAGED':
    ensure =>  'absent'
  }
  exec { "/usr/bin/dpkg-divert --rename --add /usr/lib/$(py3versions -d)/EXTERNALLY-MANAGED":
  }
# dpkg-divert --rename --add /usr/lib/$(py3versions -d)/EXTERNALLY-MANAGED

  $srcdir = '/usr/local/src'

  package { 'meson':
      ensure   => present,
      name     => 'meson',
      provider => 'pip3',
    } 

  package { 'obs-websocket-py-pip3':
    ensure   => present,
    name     => 'obs-websocket-py',
    provider => 'pip3',
  } 
  package { 'obs-wws-rc-pip3':
    ensure   => present,
    name     => 'obs-ws-rc',
    provider => 'pip3',
  }
  package { 'simpleobsws':
    ensure   => present,
    provider => 'pip3',
  }
  package { 'pulsectl-asyncio':
    ensure   => present,
    provider => 'pip3',
  }
  package { 'tornado':
    ensure   => present,
    provider => 'pip3',
  }
  package { 'asyncio':
    ensure   => present,
    provider => 'pip3',
  }
   package { 'aiohttp':
    ensure   => present,
    provider => 'pip3',
  }
  package { 'chardet':
    ensure   => present,
    provider => 'pip3',
  }
   package { 'multidict':
    ensure   => present,
    provider => 'pip3',
  }
  package { 'yarl':
    ensure   => present,
    provider => 'pip3',
  }
  if ! $facts['os']['distro']['codename']  == 'trixie' { 
    package { 'aio-timers':
      ensure   => present,
      provider => 'pip3',
    }
  }
  if ! $facts['os']['distro']['codename']  == 'trixie' { 
   
  package { 'mido':
    ensure   => present,
    provider => 'pip3',
  }
  }
  package { 'tinydb':
    ensure   => present,
    provider => 'pip3',
  }
  package { 'websocket-client':
    ensure   => present,
    provider => 'pip3',
  }
  #  package { 'python-rtmidi':
  #  ensure   => present,
  #  provider => 'pip3',
  #}
  package { 'dbj':
    ensure   => present,
    provider => 'pip3',
  }
   






}

