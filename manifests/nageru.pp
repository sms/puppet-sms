# software build from source
class sms::nageru
(
  String $user = 'user',
  String $userdir = '/home/user',

)

{
  $srcdir = '/usr/local/src'
Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


$pkgs = [
#nageru
    'libqt5printsupport5',
    'libsdl2-image-dev',
    'libbmusb-dev',
    #    'libbmusb5',
    'libeigen3-dev',
    'libepoxy-dev',
    'libgcrypt20-dev',
    'libgmp-dev',
    'libgmpxx4ldbl',
    'libgnutls-dane0',
    'libgnutls-openssl27',
    'libgnutls28-dev',
    'libgpg-error-dev',
    'libidn11-dev',
    'liblua5.2-0',
    'liblua5.2-dev',
    'liblua5.3-dev',
    'liblua5.1-dev',
    'liblua5.1-0-dev',
    'liblua50-dev',
    'libmicrohttpd-dev',
    'libmicrohttpd12',
    'libmovit-dev',
    'libmovit8',
    'libp11-kit-dev',
    'libpci-dev',
    'libprotobuf-dev',
    #'libprotobuf-lite17',
    #'libprotobuf17',
    #'libprotoc17',
    'libreadline-dev',
    'libtasn1-6-dev',
    'libunbound8',
    'libunbound-dev',
    'libzita-resampler-dev',
    'libzita-resampler1',
    'nettle-dev',
    'protobuf-compiler',
    'googletest',
    'libgtest-dev',
    'libjpeg62-turbo-dev',
    'libjpeg-dev',
    'libsqlite3-dev',
    'libqcustomplot-dev',
    #'nageru',
    #'futatabi',
    #editing:
    'kdenlive',
    'ebumeter',
    'libusb-1.0-0-dev',
    'libusb-1.0-doc'
]

  ensure_packages($pkgs, {ensure => 'installed'}) 



vcsrepo { "${srcdir}/movit":
    ensure   => present,
    provider => git,
    #  revision => '6b5ea90eace8f822ea158d4d15e0d0214d1a511e',
    source   => 'http://git.sesse.net/movit',
  }
  -> exec { 'movit autogen':
    command     => 'bash autogen.sh',
    cwd         => "${srcdir}/movit",
    subscribe   => Vcsrepo["${srcdir}/movit"],
    refreshonly => true,

  }
  -> exec { 'movit make':
    command     => 'make',
    cwd         => "${srcdir}/movit",
    subscribe   => Vcsrepo["${srcdir}/movit"],
    refreshonly => true,
  }
  -> exec { 'movit make install':
    command     => 'make install',
    cwd         => "${srcdir}/movit",
    subscribe   => Vcsrepo["${srcdir}/movit"],
    refreshonly => true,
  }

# $cefname = 'cef_binary_73.1.12+gee4b49f+chromium-73.0.3683.75_linux64_minimal.tar.bz2'
# $cefurl =  'http://opensource.spotify.com/cefbuilds/cef_binary_73.1.12%2Bgee4b49f%2Bchromium-73.0.3683.75_linux64_minimal.tar.bz2'

  $cefurl = 'http://opensource.spotify.com/cefbuilds/cef_binary_77.1.8%2Bg41b180d%2Bchromium-77.0.3865.90_linux32_minimal.tar.bz2'
  $cefname ='cef_binary_77.1.8+g41b180d+chromium-77.0.3865.90_linux32_minimal.tar.bz2'

  #file {"${srcdir}/cef":
  #  ensure => directory
  #}
  #-> exec {'fetch cef':
  #  command => "wget -O ${cefname} ${cefurl}" ,
  #  cwd     => "${srcdir}/cef",
  #  creates => "${srcdir}/cef/${cefname}"
  #}
  #-> exec {'unpack cef':
  #  command => "tar -xf ${cefname}",
  #  cwd     => "${srcdir}/cef"
  #}



  vcsrepo { "${srcdir}/nageru":
    ensure   => present,
    provider => git,
    #  revision => '6b5ea90eace8f822ea158d4d15e0d0214d1a511e',
    source   => 'http://git.sesse.net/nageru',
  }
  -> exec { 'nageru meson':
    #    command => 'meson obj -Dcef_dir=/usr/lib/x86_64-linux-gnu/cef Dcef_build_type=system -Dcef_no_icudtl=true',
    command     => "meson obj -Dcef_dir=${srcdir}/cef/${cefname} -Dcef_no_icudtl=true",
    cwd         => "${srcdir}/nageru",
    subscribe   => Vcsrepo["${srcdir}/nageru"],
    refreshonly => true,

  }
  -> exec { 'nageru ninja':
    command     => 'ninja',
    cwd         => "${srcdir}/nageru/obj",
    subscribe   => Vcsrepo["${srcdir}/nageru"],
    refreshonly => true,

  }


}
