# libndi5
class sms::ndigst
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  include sms::rust

  $pkgs = [
    'gstreamer1.0-plugins-base-apps',
    'build-essential',
    'pkg-config',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
     'gstreamer1.0-plugins-base'
    
  ]
  ensure_packages($pkgs, {ensure => 'installed'}) 


  put::buildfromgit { 'gst-plugin-ndi':
    revision => 'master',

    #srcdir => "/usr/local/src",
    repo      => 'https://github.com/teltek/gst-plugin-ndi.git',
    commands  => [
      '/root/.cargo/bin/cargo build',
      '/root/.cargo/bin/cargo build --release',
      'install -o root -g root -m 644 target/release/libgstndi.so /usr/lib/x86_64-linux-gnu/gstreamer-1.0/',
      'ldconfig'
    ]
  } 


}
