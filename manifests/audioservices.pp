# sets up jack, and pulseaudio unit files
class sms::audioservices (
  String $user = 'user',
  String $userdir = '/home/user',
  Boolean $pipewire = true,
  String  $jack_cmdline = lookup('sms::audio::jack_cmdline', undef, undef, '-d alsa'),

  )
  {
 
    require sms::etcdefault

    #depends on whether pipewire is used or not
    if ( $pipewire == true ) {
      # just there cause things depend on it
      put::userunit { 'jack':
        command       => "/usr/bin/sleep infinity",
        user          => 'user',
        autorestart => true,
        after        => [ 'pipewire.service' ],
        partof      => [ 'xsession.target' ],
        wantedby    => [ 'xsession.target' ],
     
      }

    -> put::userunit { 'jack_pulse':
      ensure => absent,
    }
    -> put::userunit { 'pulse_autoconnect':
      ensure => absent,
    }

    #this make sure pulseaudio will not be started
    -> file { "${userdir}/.config/systemd/user/pulseaudio.socket":
      ensure => link,
      target => '/dev/null',
    }
    -> file { "${userdir}/.config/systemd/user/pulseaudio.service":
      ensure => link,
      target => '/dev/null',
    }
    # those files are links to /dev/null, that prevent pipewire from starting
    -> file { '/etc/systemd/user/pipewire.socket':
      ensure => absent
    }

    -> file { '/etc/systemd/user/pipewire.service':
      ensure => absent
    }


      #put::userunit { 'pulseaudio':
      #  ensure => absent
      #}
      put::userunit { 'pulseup':
        command       => "/usr/bin/sleep infinity",
        user          => 'user',
        after        => [ 'pipewire.service', 'pipewire-pulse.service' ],
        partof      => [ 'xsession.target' ],
        wantedby    => [ 'xsession.target' ],
      }

      put::userunit { 'pipewire-pulse':
        command  => "/usr/bin/pipewire-pulse",
        user     => 'user',
        after    => [ 'pipewire.service' ],
        wants    => [ 'pipewire.service' ],
        wantedby => [ 'default.target' ],
      }
      put::userunit { 'wireplumber':
        command  => "/usr/bin/wireplumber",
        user     => 'user',
        after    => [ 'pipewire.service' ],
        wants    => [ 'pipewire.service' ],
        wantedby => [ 'default.target' ],
      }

   
    } else {  # use jackd + pulseaudio instead of pipewire

    file {  '/etc/ld.so.conf.d/pipewire-jack-x86_64-linux-gnu.conf':
      ensure => absent
      #source => 'file:///usr/share/doc/pipewire/examples/ld.so.conf.d/pipewire-jack-x86_64-linux-gnu.conf'
    }


    file { '/usr/local/src/pipewire':
      ensure => 'directory'
    }
    # pipewire does weird stuff to libjack, this will remove those things, and have a normal libjack again
    exec { 'make uninstall pipewire':
      command => '/usr/bin/make uninstall',
      cwd     => '/usr/local/src/pipewire',
      onlyif  =>  '/usr/bin/ls /usr/local/src/pipewire/Makefile'
    }

      #this make sure pulseaudio will be started
    file { "${userdir}/.config/systemd/user/pulseaudio.socket":
      ensure => absent,
    }
    -> file { "${userdir}/.config/systemd/user/pulseaudio.service":
      ensure => absent,
    }

    # those files are links to /dev/null, that prevent pipewire from starting
    -> file { '/etc/systemd/user/pipewire.socket':
      ensure => link,
      target => '/dev/null',
 
    }

    -> file { '/etc/systemd/user/pipewire.service':
      ensure => link,
      target => '/dev/null',
 

    }



      put::userunit { 'pipewire-pulse':
        ensure => absent
      }
      put::userunit { 'wireplumber':
        ensure => absent
      }
      put::userunit { 'jack':
        command       => "/usr/bin/jackd ${lookup(sms::audio::jack_cmdline)}",
        environment   => ['JACK_NO_AUDIO_RESERVATION=1'],
        user          => 'user',
        beforewtf        => [ 'pulseaudio.service' ],
        #execstartpre  => ['-/usr/bin/killall jackd', '-/usr/bin/killall jackdbus', '-/usr/bin/pactl unload-module module-alsa-card'],
        #execstartpre => ['sleep 5'],
        #execstartpost => ['sleep 5'],
        partof      => [ 'xsession.target' ],
        wantedby    => [ 'xsession.target' ],
     
      }

      put::userunit { 'pulseup':
        command       => "/usr/bin/sleep infinity",
        user          => 'user',
        after        => [ 'pulseaudio.service', 'jack_pulse.service', 'pulse_autoconnect.service' ],
        partof      => [ 'xsession.target' ],
        wantedby    => [ 'xsession.target' ],

      }

      put::userunit { 'jack_pulse':
        environment   => ['JACK_NO_AUDIO_RESERVATION=1'],
        user          => 'user',
        #autostart     => true,
        autorestart   => true,
        execstartpre  => [ "/home/user/src/streaming-media-stuff/jack_pulse_setup.sh" ],
        command       => "/usr/bin/sleep infinity",
        after   => [ 'jack.service', 'pulse.service', 'pulse_autoconnect.service' ],
        wants   => [ 'jack.service', 'pulse.service', 'pulse_autoconnect.service' ],
        #partof  => [ 'jack.service'],
        #wantedby  => [ 'jack.service'],
        #partof      => [ 'xsession.target' ],
        wantedby    => [ 'xsession.target' ],


      }
      
      #daemon, needs to start after pulse, before jack_pulse
      put::userunit { 'pulse_autoconnect':
        command => '/home/user/src/streaming-media-stuff/pulse_autoconnect.py',
        user    => 'user',
        autorestart => true,
        after   => [ 'jack.service', 'pulseaudio.service' ],
        wants   => [ 'jack.service', 'pulseaudio.service' ],
        #partof      => [ 'xsession.target' ],
        wantedby    => [ 'xsession.target' ],


      }




    }
  }
