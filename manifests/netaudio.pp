# net audio zita njbridge
class sms::netaudio (
  )
  {
   
  $pkgs = [
    'zita-njbridge',
  ]

  ensure_packages($pkgs, { ensure => 'installed' })



  hiera_hash('netbridge').each |String $name, Hash $settings| {

  if $settings[direction] == "send" {
    put::userunit { "na_send_${name}":
      command => "/usr/bin/zita-j2n --jname na_${name} --chan ${settings[channels]} ${settings[ip]} ${settings[port]} ${lookup(sms::avbif, undef, undef, 'eth0')}  ",
      user    => 'user',
      after   => [ 'jack.service', 'pulseup.service', 'jack_pulse.service' ],
      wants   => [ 'jack.service', 'pulseup.service', 'jack_pulse.service' ],
      autorestart => true,
      #partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],
    }
  }
  if $settings[direction] == "recv" {
     put::userunit { "na_recv_${name}":
      command => "/usr/bin/zita-n2j --chan ${settings[channels]} --jname na_${name} --buff ${lookup(sms::netaudio::buff)} ${settings[ip]} ${settings[port]} ${lookup(sms::avbif, undef, undef, 'eth0')} ",
      user    => 'user',
      after   => [ 'jack.service', 'pulseup.service', 'jack_pulse.service' ],
      wants   => [ 'jack.service', 'pulseup.service', 'jack_pulse.service' ],
      autorestart => true,
      #partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],
    }
  } 

  }

  file { '/etc/alsa/conf.d/100-avb.conf':
    content => epp("${module_name}/100-avb.conf.epp"), 
  }



 
}
