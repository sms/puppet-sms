# soundux soundboard
class sms::casparcg
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'

  $pkgs = [
   'build-essential',
  'cmake',
  'git',
  'g++',
  'libglew-dev', 
  'libfreeimage-dev',
  'libtbb-dev',
  'libsndfile1-dev',
  'libopenal-dev',
  'libjpeg-dev',
  'libxcursor-dev',
  'libxinerama-dev',
  'libxi-dev',
  'libsfml-dev',
  'libvpx-dev',
  'libwebp-dev',
  'liblzma-dev',
  'libmp3lame-dev',
  'libopus-dev',
  'libtheora-dev',
  'libx264-dev',
  'libx265-dev',
  'libbz2-dev',
  'libcrypto++-dev',
  'librtmp-dev',
  'libgmp-dev',
  'libxcb-shm0-dev',
  'libass-dev',
  'libgconf2-dev',
  'libopencore-amrwb-dev',
  'libsnappy-dev',
  'libopenjp2-7-dev',
  'libshine-dev',
  'libspeex-dev',
  'libtwolame-dev',
  'libvo-amrwbenc-dev',
  'libwavpack-dev',
  'libxvidcore-dev',
  'libsoxr-dev',
  'libxv-dev',
  'libxml2-dev',
  'libopenmpt-dev',
  'libbluray-dev',
  'libasound-dev',
  'libsdl2-dev',
  'libxtst-dev',
  'libatspi2.0-0',
  'libpangocairo-1.0',
  'libatk-bridge2.0-dev',
  'libxcomposite-dev',
   'vlc',
   'libvlc-dev',
   'libvlccore-dev'
  ]

  ensure_packages($pkgs, {ensure => 'installed'}) 

  put::buildfromgit { 'casparcg-media-scanner':
    revision => 'master',
    repo     => 'https://github.com/nrkno/sofie-casparcg-media-scanner.git',
    commands => [
      'yarn install'
    ]
  }
  #  put::buildfromgit { 'casparcg-server':
  #  revision => 'master',
  #  #srcdir => "/usr/local/src",
  #  repo      => 'https://github.com/CasparCG/server.git',
  #  #cwd       => './build',
  #  # https://github.com/CasparCG/server/blob/master/BUILDING.md
  #  commands  => [
  #    './tools/linux/build-in-docker',
  #    './tools/linux/extract-from-docker',
  #    'chmod -R a+r casparcg_server',
  #  ]
  #} 

  # have to figure out how to build it, but this works:
  # DISPLAY=:0 docker run  -v /dev/dri:/dev/dri   -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v /home/user/.Xauthority:/root/.Xauthority  -e "DISPLAY=unix:0.0" wegostreaming/casparcg-server:1.1
    docker::run { 'casparcg-server':
      image   => 'wegostreaming/casparcg-server:1.1',
      env     => [  
                  "DISPLAY=unix:0.0",

                 ],
      volumes => [ 
        '/dev/dri:/dev/dri',
        '/tmp/.X11-unix:/tmp/.X11-unix',
        '/home/user/.Xauthority:/root/.Xauthority',
      ],
      ports   => [
        '5250:5250',
     ],
    }



    put::userunit {'casparcg':
      command => '/usr/local/src/casparcg-server/casparcg',
      cwd     => '/home/user',
      user    => 'user',
      after       => [ 'pulseup.service'],
      partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],
    }

   file { '/opt/casparcg_client':
    ensure => 'directory',
   }

   archive { '/opt/casparcg_client.tar.gz':
    ensure        => present,
    extract       => true,
    extract_path  => '/opt/casparcg_client/',
    extract_command => '/usr/bin/tar xfz %s --strip-components=1',
    source        => 'https://versaweb.dl.sourceforge.net/project/casparcg/CasparCG_Client/CasparCG_Client_2.0/CasparCG_Client_2.0.8_Linux.tar.gz',
    creates       => '/opt/casparcg_client/run.sh',
    #cleanup       => true,
  }


}

