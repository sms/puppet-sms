# aja video driver
class sms::aja
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

 $pkgs = [
   'dkms'
 ]
 ensure_packages($pkgs, {ensure => 'installed'}) 



 
  put::buildfromgit { 'libajantv2-kernel':
    repo     => 'https://github.com/aja-video/libajantv2.git',
    revision => 'release',
    cwd      => 'driver/linux',
    commands => [
      'make',
      'make dkms-install'
    ]
  }
  
  put::buildfromgit { 'libajantv2-library':
    repo     => 'https://github.com/aja-video/libajantv2.git',
    revision => 'release',
    #cwd      => 'libajantv2/driver/linuxmake'
    commands => [
      'cmake -S . -B build',
      'cmake --build build'
    ]
  }

  put::buildfromgit { 'gst-aja':
    repo     => 'https://github.com/centricular/gst-aja.git',
    revision => 'main',
    commands => [
      'meson --prefix=/usr build',
      'ninja -C build',
      'ninja -C build install',
    ]
  }
 

}
