# various testsrces for development
class sms::testsrc (
  String $user = 'user',
  String $userdir = '/home/user',
  )
  {

  $srcdir = '/root/src'


  $pkgs = [
    'jack-tools',
  ]



  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  ensure_packages($pkgs, {ensure => 'installed'})


  put::userunit { 'ndi_testsrc':
    command   => '/home/user/src/streaming-media-stuff/scripts/ndi_testsrc.sh',
    user      => 'user',
    autorestart => true,
    after     => [ 'pulseup.service' ],
    wants     => [ 'pulseup.service' ],
    #partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }

  put::userunit { 'srt_test':
    command   => '/home/user/src/streaming-media-stuff/scripts/srt_test_publish.sh',
    user      => 'user',
    autorestart => true,
    after     => [ 'pulseup.service' ],
    wants     => [ 'pulseup.service' ],
    #partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }


  put::userunit { 'srt_test1':
    command   => '/home/user/src/streaming-media-stuff/scripts/srt_test1_publish.sh',
    user      => 'user',
    autorestart => true,
    after     => [ 'pulseup.service' ],
    wants     => [ 'pulseup.service' ],
    #partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }



    
}
