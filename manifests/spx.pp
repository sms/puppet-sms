#spx
class sms::spx
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'

  require put::nodejs
  $pkgs = [
   'build-essential',
  ]

  ensure_packages($pkgs, {ensure => 'installed'}) 

  put::buildfromgit { 'spx':
    revision => 'master',
    #srcdir => "/usr/local/src",
    repo      => 'https://github.com/TuomoKu/SPX-GC.git',
    commands  => [
      'npm install',
    ]
  } 

    put::userunit {'spx':
      command => 'node server.json relative/path/to/config.json',
      cwd     => '/usr/local/src/spx',
      user    => 'user',
      after       => [ 'pulseup.service'],
      partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],
    }

}

