# v4l2sink setup, used by obs
class sms::v4l2sink (
  String $user = 'user',
  String $userdir = '/home/user',
  )
  {
  tag 'sms'
  require put::debian


 $pkgs = [
    'v4l2loopback-dkms',
  ]
  ensure_packages($pkgs, {ensure => 'installed'})




$v4l2loopback = @("EOF")
[Unit]
Description=Setup v4lsink
#After=network.target

[Service]
Type=oneshot
ExecStart=modprobe v4l2loopback exclusive_caps=1 video_nr=9 card_label="obs"

RemainAfterExit=true
ExecStop=rmmod v4l2loopback
StandardOutput=journal

[Install]
WantedBy=multi-user.target
EOF

  file{ "/etc/systemd/system/v4l2loopback.service":
    ensure => file,
    owner  => 'root',
    content => $v4l2loopback, 
  }





}

