# audio plugins
class sms::audioplugins {
  tag 'sms'

  require put::debian
  require sms::molot
  #require sms::synth

  $pkgs = [
#utilities
    'zita-lrx',
    'zita-mu1',
    'zita-njbridge',
    'zita-ajbridge',
    'zita-rev1',
    'zita-at1',
    'ebumeter',
    'bitmeter',
    'jkmeter',
    'jmeters',
    'meterbridge',
    'ecasound',
    'ecatools',
    'nama',
    #'mocp',

#plugins
    'calf-plugins',
    #'dpf-plugins',
    #'freaked-plugins',
    #'tal-plugins',
    #'artyfx',
    #'drowaudio-plugins',
    #'infamous-plugins',
    'invada-studio-plugins-lv2',
    #'leet-plugins',
    'mda-lv2',
    #'mod-utilities',
    #'psi-plugins',
    'ste-plugins',
    'swh-lv2',
    'tap-plugins',
    #'teragonaudio-plugins',
    #'vcf-plugins',
    'x42-plugins',
    'zam-plugins',
    'eq10q',
    #'file-plugins',
# audio players
  'vlc',
  'vlc-plugin-jack',
  #magnetophon
  'puredata',
  'faust',
  'ladspa-sdk',
  'lv2-c++-tools',
  'libgtk2.0-dev',
  'libgraphene-1.0-dev',
  'libzita-convolver-dev',
  #plugins
  'ir.lv2',
  'libvterm-dev',

  ]

  ensure_packages($pkgs, {ensure => 'installed'})

  put::buildfromgit { 'master_me':
    repo      => 'https://github.com/trummerschlunk/master_me.git',
    revision  => 'master',
    commands  => [
      'make',
      'make install'
    ]
  }

  #Notice: /Stage[main]/Sms::Audioplugins/Put::Buildfromgit[voiceoffaust]/Exec[voiceoffaust: make]/returns: lv2.cpp:294:10: fatal error: lv2/lv2plug.in/ns/lv2core/lv2.h: No such file or directory
 

}

