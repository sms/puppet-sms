#pis
class sms::moreheatpi
(
  String $user = 'user',
  String $userdir = '/home/user',
)

{
  require put::debian
  require sms::moreheatcommon

  put::userunit { 'mhplay':
    command     => '/home/user/src/More-Heat-Than-Light/run-boot.sh',
    user        => 'user',
    autostart   => true,
    autorestart => true,
    after       => [ 'dbus.service' ],
    wants       => [ 'dbus.service' ],
    partof      => [ 'dbus.service' ],
    wantedby    => [ 'dbus.service' ],
  }


  put::userunit { 'mhtemp':
    command     => '/home/user/src/More-Heat-Than-Light/run-boot-temp.sh',
    user        => 'user',
    autostart   => true,
    autorestart => true,
    after       => [ 'dbus.service' ],
    wants       => [ 'dbus.service' ],
    partof      => [ 'dbus.service' ],
    wantedby    => [ 'dbus.service' ],
  }


}
