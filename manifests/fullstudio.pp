# full studio desktop
class sms::fullstudio
(
)

{
  include put::debian
  include put::ssh
  include sms::audio
  include put::xfdesktop
  include sms::vnc

  include sms::obs
  include sms::chat 
  include sms::ardour
#  include sms::mumble
  include put::sshhs
  include put::signal
  include sms::cloud

  include sms::jack
}


