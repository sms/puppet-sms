# shmdata
class sms::shmdata
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
   require sms::ndi5

   $pkgs = [
     'cmake',
     'build-essential',
     'git',
     'libgstreamer1.0-dev',
     'libgstreamer-plugins-base1.0-dev',
     'python3-dev'
   ]

  package { 'pyopengl':
      ensure   => present,
      name     => 'pyopengl',
      provider => 'pip3',
    } 

  if ! $facts['os']['distro']['codename']  == 'trixie' { 
 
   put::buildfromgit { 'shmdata':
    repo     => 'https://gitlab.com/sat-mtl/tools/shmdata.git',
    revision => '1.3.74',
    cwd      => 'build',
    commands => [
      'cmake -DTEST_COVERAGE=FALSE -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release .. ', 
      'make',
      'make install',
      'ldconfig',
    ],
  }
}
 
  put::buildfromgit { 'ndi2shmdata':
    repo     => 'https://gitlab.com/sat-metalab/ndi2shmdata.git',
    revision => 'master',
    cwd      => 'build',
    commands => [
      'cmake -DACCEPT_NDI_LICENSE=ON ..', 
      'make',
      'make install'
    ],
  }

  put::buildfromgit { 'blender-addon-shmdata':
    repo     => 'https://gitlab.com/sat-mtl/tools/blender-addon-shmdata.git',
    revision => 'master',
    commands => [
      './build.sh', 
    ],
  }
}

 


#deps:
 # https://gitlab.com/sat-metalab/ndi2shmdata  
 #  https://ossia.io/score-docs/devices/shmdata-devices.html
 #  https://gitlab.com/sat-metalab/shmdata/
 #git clone https://gitlab.com/sat-metalab/ndi2shmdata.git
 #cd ndi2shmdata && mkdir build && cd build
 #cmake -DACCEPT_NDI_LICENSE=ON ..
 #make
 #sudo make install


 #appimage https://github.com/ossia/score/releases/download/v3.0.8/ossia.score-3.0.8-linux-amd64.AppImage
 
 # source build on debian: https://ossia.io/score-docs/development/build/release.html

