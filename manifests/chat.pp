# chat (hexchat)
class sms::chat
(
  String $user = 'user',
  String $userdir = '/home/user',
  String $ircprefix = "x"

)

{
  $pkgs = [
    'hexchat',
  ]

  require put::debian



  ensure_packages($pkgs, {ensure => 'installed'})

  file{ "${userdir}/.config/hexchat/":
    ensure => directory,
    owner  => 'user'
  }


  $hexchatrc = @("EOF")

#hexchat
version = 2.12.4
irc_logging = 0
gui_slist_skip = 1
irc_nick1 = ${ircprefix}_${facts['networking']['hostname']}
irc_nick2 = ${ircprefix}_${facts['networking']['hostname']}_1
irc_nick3 = ${ircprefix}_${facts['networking']['hostname']}_2
irc_nick_hilight =
irc_no_hilight = NickServ,ChanServ,InfoServ,N,Q
EOF

  file{ "${userdir}/.config/hexchat/hexchat.conf":
    content => $hexchatrc,
    notify  => Put::Userunit['hexchat']
  }
#FIXME: intergalatic needs autosendcmd, and no ssl
  $hexchatservlist = @("EOF")
N=intergalactic
E=UTF-8 (Unicode)
F=127
D=0
S=chat.intergalactic.fm/6667
J=#town-square


N=hackint
E=UTF-8 (Unicode)
F=127
D=0
S=irc.hackint.org/6697
J=#papillon
J=#tbd
J=#proxycafe

N=indymedia
E=UTF-8 (Unicode)
F=127
D=0
S=irc.indymedia.org/6697
J=#papillon
J=#lag

N=IRCNet
E=UTF-8 (Unicode)
F=19
D=0
S=open.ircnet.net

N=OFTC
E=UTF-8 (Unicode)
F=19
D=0
S=irc.oftc.net

N=freenode
L=6
E=UTF-8 (Unicode)
F=23
D=0
S=chat.freenode.net
S=irc.freenode.net
EOF

  file{ "${userdir}/.config/hexchat/servlist.conf":
    content => $hexchatservlist,
    notify  => Put::Userunit['hexchat']
  }



  put::userunit { 'hexchat':
    command     => '/usr/bin/hexchat --minimize=2',
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'xsession.target' ],
    wants       => [ 'xsession.target' ],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }


}
