# speaker measurement
class sms::measurement
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{

  tag 'slow', 'building_software', 'sms'

  require put::debian
  require sms::audio


  $pkgs = [
    'zlib1g-dev',
    'libcurl4-openssl-dev',
    'libfftw3-dev',
    'libfontconfig1-dev',
    'libgl-dev',
    'libmpg123-dev',
    'libpulse-dev',
    'libsndfile1-dev',
    'libtag1-dev',
    'libxcursor-dev',
    'libxi-dev',
    'libxinerama-dev',
    'libxrandr-dev',
    'pkgconf',
    'qtbase5-dev',
    'qtdeclarative5-dev',
    'qtmultimedia5-dev',
    'qtquickcontrols2-5-dev',
    'qml-module-qtcharts',
    'qml-module-qtquick-shapes',
    'qml-module-qt-labs-folderlistmodel',
    'qml-module-qt-labs-settings',
    'libqt5charts5-dev',
    'libqt5svg5-dev',
    'libqt5multimedia5-plugins'
  ]

  ensure_packages($pkgs, {ensure => 'installed'})


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'

  put::buildfromgit { 'distrho-ports':
    repo     => 'https://github.com/mincequi/qLouder',
    revision => 'main',
    cwd      => 'build',
    commands => [ 
                  'cmake ..',
                  'make',
                ]
  }

}

