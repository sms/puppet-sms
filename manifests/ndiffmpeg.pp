# ffmpeg with ndi input / output support (which was removed in ffmpeg after some point)
class sms::ndiffmpeg
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'




#ffmpeg
#cd ~/ffmpeg_sources && wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 && tar xjvf ffmpeg-snapshot.tar.bz2 && cd ffmpeg && PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure --prefix="$HOME/ffmpeg_build" --pkg-config-flags="--static" --extra-cflags="-I$HOME/ffmpeg_build/include -I$HOME/ffmpeg_sources/ndi/include" --extra-ldflags="-L$HOME/ffmpeg_build/lib -L$HOME/ffmpeg_sources/ndi/lib/x86_64-linux-gnu" --extra-libs="-lpthread -lm" --bindir="$HOME/bin" --enable-gpl --enable-libass --enable-libfreetype --enable-libvorbis --enable-libx264 --enable-libx265 --enable-libndi_newtek --enable-nonfree && PATH="$HOME/bin:$PATH" 
#make -j4 && make install && hash -r

# patch for newer ndi: https://gist.github.com/pabloko/8affc8157a83e201503c326a28d17bf2
#
  put::buildfromgit { 'ffmpeg-ndi':
      repo     => 'https://github.com/FFmpeg/FFmpeg.git',
      revision => '6b5ea90eace8f822ea158d4d15e0d0214d1a511e',

      commands => [
        'bash configure --prefix=/srv/ffmpeg --extra-libs="-lpthread -lm" --enable-libvorbis --enable-libx264 --enable-libx265 --enable-nonfree --enable-gpl --enable-libass --enable-libfreetype --enable-libndi_newtek   --enable-libfdk-aac --enable-libopus --enable-libfdk-aac ',
        'make -j2',
        'make install'
        ],

  }

}
