# cef
class sms::cef
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian
  require sms::audio

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }





  #https://cdn-fastly.obsproject.com/downloads/cef_binary_5060_linux_x86_64_v3.tar.xz


  file { '/opt/cef':
    ensure => 'directory'
  }
  -> archive { '/opt/cef1.tar.xz':
    ensure          => present,
    extract         => true,
    extract_path    => '/opt/cef/',
    extract_command => '/usr/bin/tar -xf %s --strip-components=1',
    source          => 'https://cdn-fastly.obsproject.com/downloads/cef_binary_5060_linux_x86_64_v3.tar.xz',
    #    creates    => '/opt/casparcg_client/run.sh',
    #cleanup        => true,
  }
  #-> exec { 'mkdir -p /opt/cef/build':
  #}
}

