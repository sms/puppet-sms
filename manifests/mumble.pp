# mumble
class sms::mumble (
  String $user = 'user',
  String $userdir = '/home/user',
  # server/room
  String $mumbleserver = 'farma.cisti.org/test',
  )
  {
  tag 'sms'

  require put::debian
  require sms::audio


  put::buildfromgit { 'mumble':
    repo     => 'https://github.com/mumble-voip/mumble.git',
    tag      => ['mumble'],
    revision  => '1.3.3',
    cwd       => './build',
    commands => [
    'qmake CONFIG+="no-g15 no-embed-qt-translations no-server no-dbus no-pulseaudio no-oss no-alsa no-gkey " -recursive ..',
#     "cmake -Dalsa=OFF -Dpulseaudio=OFF -Dserver=OFF  ..", 
     'make', 
    ],

    # qmake CONFIG+="no-g15 no-server no-dbus no-pulseaudio no-oss no-alsa no-gkey " -recursive
  }




  file{ "${userdir}/.config/Mumble/":
    ensure => directory,
    owner  => 'user'
  }

  $mumblerc = @("EOF")
[General]
lastupdate=2

[jack]
output=2 # or 2 (mono/stereo)
autoconnect=false # or false
startserver=false # or false

[audio]
input=jack
output=jack
quality=16000

[messages]
size=29

[messagesounds]
size=29
EOF

  file{ "${userdir}/.config/Mumble/Mumble.conf.jack":
    ensure  => present,
    content => $mumblerc,
  }

  put::userunit { 'mumble_live':
    command     => "/usr/local/src/mumble/build/mumble -jn mumble-live -m mumble://${ircprefix}_${::hostname}_live@${mumbleserver}/live",
    tag         => ['mumble'],
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'jack.service', 'xsession.target'],
    wants       => [ 'jack.service', 'xsession.target'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }
  put::userunit { 'mumble_talkback':
    command     => "/usr/local/src/mumble/build/mumble -jn mumble-talkback -m mumble://${ircprefix}_${::hostname}_talkback@${mumbleserver}/talkback",
    tag         => ['mumble'],
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'jack.service', 'xsession.target'],
    wants       => [ 'jack.service', 'xsession.target'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }

}
