define sms::service (
  Integer $dport,
  String $proto = 'tcp',
  String $type = '_http._tcp'
) {


  $avahi = @("EOF")
<?xml version="1.0" standalone='no'?><!--*-nxml-*-->
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
    <name replace-wildcards="yes">%h ${name}</name>
    <service>
        <type>${type}</type>
        <port>${dport}</port>
    </service>
</service-group>
EOF

  file {"/etc/avahi/services/${name}.conf":
    content => $avahi, 
  }

 firewall { "200 ${name}":
      dport  => $dport,
      proto  => $proto,
      action => 'accept',
    }

}
