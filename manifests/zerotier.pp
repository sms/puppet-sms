class sms::zerotier {
  require put::debian

  Exec {
    user    => 'root',
    cwd     => "${srcdir}/rtl_433/build",
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  file { "/usr/local/bin/zerotier-install.sh":
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0700',
    source => "puppet:///modules/${module_name}/zerotier-install.sh",
  }


  exec { 'install':
    command => 'bash /usr/local/bin/zerotier-install.sh',
    cwd     => '/root',
  }

  exec { 'join net':
    command => '/usr/sbin/zerotier-cli join a09acf0233258011',
    cwd     => '/root',
  }


}
