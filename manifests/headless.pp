# sets up jack 
class sms::headless (
  String $user = 'user',
  String $userdir = '/home/user',

  )
  {

  require put::userunitprep
  require put::debian
  require put::base
  require put::ssh


  $xsession_unit = @(EOF)
[Unit]
Description=Xsession running (not really)

EOF

  file { "/home/${user}/.config/systemd/user/xsession.target":
    content => $xsession_unit,
  }

 
      put::userunit { 'quasyheadless':
        user          => 'user',
        command       => "systemctl --user start xsession.target",
        wantedby    => [ 'default.target' ],

      }

    }
  
