# installs bunch of sdr tools
class sms::sdr {

  include git
  $pkgs = [
    'circus',
    'build-essential',
    'cmake',
    'gr-osmosdr',
    'qt5-qmake',
    'libusb-1.0-0-dev',
    'libqt4-dev',
    'libpulse-dev',
    ##'python-flower',
    'php-bcmath',
    'composer',
    'php-amqp',
    'python3-setuptools',
    'rabbitmq-server',
    'redis-server',
    'build-essential',
    'libtool',
    'automake',
    'autoconf',
    'librtlsdr-dev',
    'libfftw3-dev',
    'screen',
    'python3-pip',
    'python3-numpy',
    'python3-dev',
    'python3-scipy',
    'python3-urllib3',
    'python3-prompt-toolkit',
    #'python-gps',
    'libhackrf-dev',
    'libhackrf0',
    'python3-zmq',
    'librtlsdr-dev',
    'rtl-sdr',
    'soapysdr-module-rtlsdr',
    'hackrf',
    'libhackrf-dev',
    'gr-air-modes',
  # 'gr-gsm',
    'gnuradio',
    'gnuradio-dev',
    'gr-osmosdr',
    'gr-iqbal',
  # 'gr-dab',
    'libhamlib2',
    'libhamlib-dev',
  # 'wsjtx',
    'hackrf',
    #'python-numpy',
    'libtool',
    'libncurses5-dev',
    'build-essential',
    'autoconf',
    'automake',
    'vorbis-tools',
    'sox',
    'alsa-utils',
    'unzip',
    'libxml2-dev',
    'socat',
  # 'lib32gcc-8-dev',
  # 'lib32gcc-6-dev',
    'libtalloc-dev',
    'libpcsclite-dev',
    ##'realpath',
    'sqlite3',
    'sox',
    'sudo',
    'libfftw3-dev',
    'libtclap-dev',
    'librtlsdr-dev',
    'pkg-config',
    'libjack-jackd2-dev',
    'libqt5multimedia5',
    'libqt5xdg-dev',
    'libqt5gstreamer-dev',
    #'qt5core-dev',
    #'qtbase6-dev',
    #'qtbase5-gles-dev',
    'protobuf-compiler',
    'protobuf-c-compiler',
    'manpages-pl',
    'libqt5core5a',
    'libpackagekitqt5-dev',
    #'python-pyqt5.qtmultimedia',
    'python3-pyqt5.qtmultimedia',
    'qtmultimedia5-dev',
    'swig3.0',
    'libjansson-dev',
    'libprotobuf-c-dev',
    'nasm',
    'yasm',
    'libpulse-dev',
    'libconfig++-dev',
    'libshout3-dev'

  ]

  ensure_packages($pkgs, {ensure => 'installed'})

#BUG


$rtlpkgc = @("EOF")

prefix=/usr
exec_prefix=${prefix}
libdir=${exec_prefix}/lib
includedir=${prefix}/include

Name: RTL-SDR Library
Description: C Utility Library
Version: 0.6.0
Cflags: -I${includedir}/
Libs: -L${libdir} -lrtlsdr
Libs.private:  -lusb-1.0 
@audio   -  rtprio     95
@audio   -  memlock    unlimited
@audio   -  nice      -19

EOF

  file { '/usr/lib/x86_64-linux-gnu/pkgconfig/librtlsdr.pc':
    content => $rtlpkgc,
  }




  $srcdir = '/root/src'

  Exec {
    user    => 'root',
    cwd     => "${srcdir}/rtl_433/build",
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  exec { 'ldconfig':
      command => 'ldconfig',
    }


  $sname1 = 'concurrencykit'
  vcsrepo { "${srcdir}/${sname1}":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/concurrencykit/ck.git',
  }
  -> exec { "${sname1} configure ":
    command => 'bash ./configure',
    cwd     => "${srcdir}/${sname1}",
  }
  -> exec { "${sname} make ":
    command => 'make',
    cwd     => "${srcdir}/${sname1}",
  }   -> exec { "${sname1} make install ":
    command => 'make',
    cwd     => "${srcdir}/${sname1}",
  }

  $sname2 = 'tsl'
  vcsrepo { "${srcdir}/${sname2}":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/pvachon/tsl.git',
  }
  -> exec { "${sname2} configure ":
    command => "${srcdir}/${sname2}/waf configure",
    cwd     => "${srcdir}/${sname2}",
  }
  -> exec { "${sname2} make install":
    command => "${srcdir}/${sname2}/waf  build install",
    cwd     => "${srcdir}/${sname2}",
  }

  $sname = 'tsl-sdr'
  vcsrepo { "${srcdir}/${sname}":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/pvachon/tsl-sdr.git',
  }
  -> exec { "${sname} configure ":
    command => "${srcdir}/${sname}/waf  configure",
    cwd     => "${srcdir}/${sname}",
  }
  -> exec { "${sname} waf build":
    command => "${srcdir}/${sname}/waf build",
    cwd     => "${srcdir}/${sname}",
  }




  vcsrepo { '/home/user/src/streaming-media-stuff/':
    ensure     => latest,
    provider   => git,
    submodules => false,
    remote     => origin,
    source     => {
      'origin' => 'http://git.puscii.nl/sms/streaming-media-stuff.git',
      'gl'     => 'git@185.52.224.4:stuff/cellery.git'
    }
  }

  vcsrepo { "${srcdir}/gps-sdr-sim":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/osqzss/gps-sdr-sim.git',
  }
  -> exec { 'gps-sdr-sim make ':
    command => 'gcc gpssim.c -lm -O3 -o gps-sdr-sim',
    cwd     => "${srcdir}/gps-sdr-sim",
  }
  -> exec { 'install ':
    command => 'cp gps-sdr-sim /usr/local/bin',
    cwd     => "${srcdir}/gps-sdr-sim",
  }



  vcsrepo { "${srcdir}/rtl_433":
    ensure   => latest,
    provider => git,
    source   => 'https://github.com/merbanan/rtl_433.git',
  }
  -> file { "${srcdir}/rtl_433/build":
    ensure => directory,
  }
  -> exec { 'rtl433 cmake ..':
    command => 'cmake ..',
  }
  -> exec { 'rtl433 make':
    command => 'make',
  }
  -> exec { 'rtl433 make install':
    command => 'make install',
  }

  vcsrepo { "${srcdir}/rtl-power-fftw":
    ensure   => latest,
    provider => git,
    source   => 'https://github.com/AD-Vega/rtl-power-fftw.git',
  }
  -> file { "${srcdir}/rtl-power-fftw/build":
    ensure => directory,
  }
  -> exec { 'rtl p fft cmake ..':
    command => 'cmake ..',
    cwd     => "${srcdir}/rtl-power-fftw/build",
  }
  -> exec { 'rtl p fft make':
    command => 'make',
    cwd     => "${srcdir}/rtl-power-fftw/build",
  }
  -> exec { 'rtl  p fft make install':
    command => 'make install',
    cwd     => "${srcdir}/rtl-power-fftw/build",
  }

  vcsrepo { "${srcdir}/rtlmic":
    ensure   => latest,
    provider => git,
    source   => 'https://github.com/marcan/rtlmic.git',
  }
  -> exec { 'rtlmic make':
    command => 'make',
    cwd     => "${srcdir}/rtlmic",
  }
  -> exec { 'rtlmic make install':
    command => 'make install',
    cwd     => "${srcdir}/rtlmic",
  }





  vcsrepo { "${srcdir}/kalibrate-rtl":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/steve-m/kalibrate-rtl.git',
  }

  exec { 'kalibrate bootstrap':
    command => "/bin/bash bootstrap && CXXFLAGS='-W -Wall -O3'",
    cwd     => "${srcdir}/kalibrate-rtl",
  }

  -> exec { 'kalibrate configure':
    command => '/bin/bash configure',
    cwd     => "${srcdir}/kalibrate-rtl",
  }


  -> exec { 'kalibrate make':
    command => 'make',
    cwd     => "${srcdir}/kalibrate-rtl",
  }

  -> exec { 'kalibrate make install':
    command => 'make install',
    cwd     => "${srcdir}/kalibrate-rtl",
  }


  ##multmon

  vcsrepo { "${srcdir}/multimon-ng":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/EliasOenal/multimon-ng.git',
  }

  -> file { "${srcdir}/multimon-ng/build":
    ensure => directory,
  }

  exec { 'mm cmake ..':
    command => 'cmake ..',
    cwd     => "${srcdir}/multimon-ng/build",

  }

  -> exec { 'mm make':
    command => 'make',
    cwd     => "${srcdir}/multimon-ng/build",

  }

  -> exec { 'mm make install':
    command => 'make install',
    cwd     => "${srcdir}/multimon-ng/build",

  }

  vcsrepo { "${srcdir}/csdr":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/simonyiszk/csdr.git',
  }

  exec { 'csdr make':
    command => 'make',
    cwd     => "${srcdir}/csdr",

  }

  -> exec { 'csdr make install':
    command => 'make install',
    cwd     => "${srcdir}/csdr",

  }



  vcsrepo { "${srcdir}/openwebrx":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/simonyiszk/openwebrx.git',
  }

  vcsrepo { "${srcdir}/shinysdr":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/kpreid/shinysdr.git',
  }
#    -> exec { 'shiny jsdeps':
#      command => 'bash fetch-js-deps.sh',
#      cwd     => "${srcdir}/shinysdr",
#    }
  -> exec { 'shiny build':
    command => 'python setup.py build',
    cwd     => "${srcdir}/shinysdr",
  }
  -> exec { 'shiny install':
    command => 'python setup.py install',
    cwd     => "${srcdir}/shinysdr",
  }
  -> exec { 'shiny configdir':
    command => '/usr/local/bin/shinysdr --create /srv/shinysdr',
    cwd     => '/root/',
    creates => '/srv/shinysdr'
  }


  vcsrepo { "${srcdir}/gr-radioteletype":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/bitglue/gr-radioteletype.git',
  }

  vcsrepo { "${srcdir}/kukuruku":
    ensure   => present,
    provider => git,
    source   => 'https://jenda.hrach.eu/p/kukuruku',
  }
  -> exec { 'make kuk client':
    command => 'make',
    cwd     => "${srcdir}/kukuruku/client"
  }
  -> exec { 'make kuk scanner':
    command => 'make',
    cwd     => "${srcdir}/kukuruku/scanner"
  }
  -> exec { 'make kuk server':
    command => 'make',
    cwd     => "${srcdir}/kukuruku/server"
  }

  vcsrepo { "${srcdir}/blobutils":
    ensure   => present,
    provider => git,
    source   => 'https://jenda.hrach.eu/p/blobutils',
  }
  -> exec { 'make blobutils jcf':
    command => 'cp jcf /usr/local/bin ; chmod +x /usr/local/bin/jcf',
    cwd     => "${srcdir}/blobutils"
  }




  vcsrepo { "${srcdir}/fcl":
    ensure     => present,
    provider   => git,
    #commit specified does not exist
    submodules => false,
    source     => 'https://jenda.hrach.eu/p/fcl',
  }
    -> exec { 'make fcl':
      command => 'make',
      cwd     => "${srcdir}/fcl"
  }

  vcsrepo { "${srcdir}/tetra-listener":
    ensure   => present,
    provider => git,
    source   => 'https://jenda.hrach.eu/p/tetra-listener',
  }
#    -> exec { 'make tetra-listener':
#      command => 'bash build.sh',
#      cwd     => "${srcdir}/tetra-listener"
#    }


  vcsrepo { "${srcdir}/install-tetra-codec":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/sq5bpf/install-tetra-codec',
  }
    -> file { '/tetra/':
      ensure => directory,
    }
    -> file { '/tetra/bin':
      ensure => directory,
    }
    -> exec { 'install tetra coded':
      command => 'bash install.sh',
      cwd     => "${srcdir}/install-tetra-codec"
    }

  vcsrepo { "${srcdir}/libosmocore-sq5bpf":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/sq5bpf/libosmocore-sq5bpf',
  }
    -> exec { 'osmocore autoreconf -i':
      command => 'autoreconf -i',
      cwd     => "${srcdir}/libosmocore-sq5bpf"
    }
    -> exec { 'osmocore configure':
      command => 'bash configure',
      cwd     => "${srcdir}/libosmocore-sq5bpf"
    }
    -> exec { 'osmocore make':
      command => 'make',
      cwd     => "${srcdir}/libosmocore-sq5bpf"
    }
    -> exec { 'osmocore make install':
      command => 'make install',
      cwd     => "${srcdir}/libosmocore-sq5bpf"
    }
    -> exec { 'osmocore ldconfig':
      command => 'ldconfig',
      cwd     => "${srcdir}/libosmocore-sq5bpf"
    }

  vcsrepo { "${srcdir}/osmo-tetra-sq5bpf":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/sq5bpf/osmo-tetra-sq5bpf',
  }
    -> exec { 'osmo tetra make':
      command => 'make',
      cwd     => "${srcdir}/osmo-tetra-sq5bpf/src"
    }

  vcsrepo { "${srcdir}/telive":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/sq5bpf/telive',
  }
    -> exec { 'telive':
      command => 'make',
      cwd     => "${srcdir}/telive"
    }
    -> exec { 'telive install':
      command => 'bash install.sh',
      cwd     => "${srcdir}/telive"
    }



  vcsrepo { '/var/www/sdr/':
    ensure   => latest,
    provider => git,
    #gitlab+deploy-token-7  WMSkLx2Tc-ppQ7NXNeKX
    source   => 'https://gitlab+deploy-token-7:WMSkLx2Tc-ppQ7NXNeKX@git.puscii.nl/stuff/sdrboxwww.git',
  }

  vcsrepo { "${srcdir}/cellery":
    ensure   => latest,
    provider => git,

    remote   => origin,
    source   => {
      'origin' => 'https://gitlab+deploy-token-8:wjCBsgyafUjPr6TwDGrj@git.puscii.nl/stuff/cellery.git',
      'gl'     => 'git@185.52.224.4:stuff/cellery.git'
    }

  }


  vcsrepo { "${srcdir}/sdrangelove":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/osmocom/sdrangelove.git',
  }
  -> file { "${srcdir}/sdrangelove/build":
    ensure => directory,
  }
  -> exec { 'sdrangelove cmake':
    #cmake ../ -DCMAKE_PREFIX_PATH=/path/to/Qt/5.2.0/gcc_64/lib/cmake/
    command => 'cmake ..',
    cwd     => "${srcdir}/sdrangelove/build"
  }
  -> exec { 'sdrangelove make':
    #cmake ../ -DCMAKE_PREFIX_PATH=/path/to/Qt/5.2.0/gcc_64/lib/cmake/
    command => 'make',
    cwd     => "${srcdir}/sdrangelove/build"
  }


  vcsrepo { "${srcdir}/rtl-ais":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/dgiardini/rtl-ais.git',
  }
  -> exec { 'rtl-ais make':
    #cmake ../ -DCMAKE_PREFIX_PATH=/path/to/Qt/5.2.0/gcc_64/lib/cmake/
    command => 'make',
    cwd     => "${srcdir}/rtl-ais"
  }
  -> exec { 'rtl-ais cp':
    #cmake ../ -DCMAKE_PREFIX_PATH=/path/to/Qt/5.2.0/gcc_64/lib/cmake/
    command => 'cp rtl_ais /usr/local/bin/',
    cwd     => "${srcdir}/rtl-ais"
  }

  vcsrepo { "${srcdir}/RTLSDR-Airband":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/szpajder/RTLSDR-Airband.git',
  }
  -> exec { 'rtl airband make':
    #cmake ../ -DCMAKE_PREFIX_PATH=/path/to/Qt/5.2.0/gcc_64/lib/cmake/
    environment => ['HOME=/root', 'PLATFORM=x86', 'NFM=1', 'PULSE=1'  ],
    command     => 'make',
    cwd         => "${srcdir}/RTLSDR-Airband"
  }  -> exec { 'rtl airband make install':
    #cmake ../ -DCMAKE_PREFIX_PATH=/path/to/Qt/5.2.0/gcc_64/lib/cmake/
    command => 'make install',
    cwd     => "${srcdir}/RTLSDR-Airband"
  }












  vcsrepo { "${srcdir}/gammarf":
    ensure   => present,
    provider => git,
    source   => 'https://github.com/gammarf/gammarf.git',
  }

  $pypkgs = [
    'pyrtlsdr',
    'gps3',
    'zmq',
    'pyModeS'
  ]
  ensure_packages( $pypkgs,
  {
    ensure   => present,
    provider => 'pip3',
  })

  #class { 'supervisord':
  #  install_init         => true,
  #  init_defaults        => true,
  #  unix_socket          => true,
  #  inet_server          => true,
  #  inet_server_hostname => '127.0.0.1',
  #  inet_server_port     => '9001',
  #  inet_auth            => false,
  #  inet_username        => undef,
  #  inet_password        => undef,
  #}

  #supervisord::program { 'openwebrx':
  #  command             => '$srcdir/openwebrx/openwebrx.py',
  #  priority            => '100',
  #  program_environment => {
  #    'HOME'   => '$srcdir/openwebrx/',
  #    'PATH'   => '/bin:/sbin:/usr/bin:/usr/sbin',
  #    'SECRET' => 'mysecret'
  #  }
  #}

  file { '/usr/local/bin/kal.sh':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0755',
    source => "puppet:///modules/${module_name}/scripts/kal.sh",
  }
}

