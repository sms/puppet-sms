# sms videodriver
class sms::videodriver (
  String  $type = 'freesoftware',
)
{

  require put::debian


$purgepkgs = [
  'glx-alternative-nvidia',
  'glx-diversions',
  'libcuda1',
  'libcuinj64-9.2',
  'libnvcuvid1',
  'libnvidia-cfg1',
  'libnvidia-ml1',
  'nvidia-alternative',
  'nvidia-cuda-dev',
  'nvidia-installer-cleanup',
  'nvidia-kernel-common',
  'nvidia-kernel-dkms',
  'nvidia-kernel-support',
  'nvidia-legacy-check',
  'nvidia-modprobe',
  'nvidia-persistenced',
  'nvidia-smi',
  'nvidia-support'
]

# not working????
#  ensure_packages($pkgs, {ensure => 'absent'})

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  exec{ 'purge nvidia':
    command => 'apt-get -y purge nvidia* *nvidia *nvidia*'
  }

}
