# sms etcdefault, so that scripts running can access settings (like hostnames, secrets, etc.)
class sms::etcdefault (
  )
{

  file { '/etc/default/sms':
    content => epp("${module_name}/sms.epp"), 
  }

}

 
