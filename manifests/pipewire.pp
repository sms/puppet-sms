# sets up pipewire, instead of jack / pulse
class sms::pipewire (
  String $user = 'user',
  String $userdir = '/home/user',
)

{
  require sms::audio
  require sms::rust

  #require sms::pipewireutils

  put::buildfromgit { 'coppwr':
    revision => 'main',

    #srcdir => "/usr/local/src",
    repo      => 'https://github.com/dimtpap/coppwr.git',
    commands  => [
      '/root/.cargo/bin/cargo build --release',
    ]
  } 


  # TODO
  # https://github.com/dimtpap/coppwr
  # https://github.com/noisetorch/NoiseTorch
  # https://sonobus.net/
  # https://github.com/noisetorch/NoiseTorch
  # https://sonobus.net/
 # https://bennett.dev/auto-link-pipewire-ports-wireplumber/

  # to restart pipewire: systemctl --user restart wireplumber pipewire pipewire-pulse
  # pacmd is broken
  # 
 
  class {'sms::audioservices':
    pipewire =>  true,
  }

  $rmpkgs = [
      'pulseaudio',
      'pulseaudio-module-jack',
  ]
  ensure_packages($rmpkgs, {ensure => 'absent'})


  $pkgs = [
    'pipewire',
    'pipewire-alsa',
    'pipewire-audio',
    'pipewire-audio-client-libraries',
    'pipewire-bin',
    'pipewire-doc',
    'pipewire-jack',
    'pipewire-libcamera',
    'pipewire-pulse',
    'pipewire-tests',
    'pipewire-v4l2',
  ]
  ensure_packages($pkgs, {ensure => 'present'})

  $remove_pkgs = [
    'pipewire-media-session',
    'pipewire-media-session-alsa',
    'pipewire-media-session-jack',
    'pipewire-media-session-pulseaudio',
  ]
  ensure_packages($remove_pkgs, {ensure => 'absent'})

  $quaconf = @("EOF")
context.properties = {
   default.clock.quantum = 2048
   default.clock.min-quantum = 2048
}
EOF

  #file { '/home/user/.config/pipewire/pipewire.conf.d/clock_quantum.conf':
  #  content => $quaconf,
  #}

  $pulsedaemon = @("EOF")
daemonize = no
EOF

  file { "/etc/pulse/daemon.conf":
    content => $pulsedaemon,
  }

  $pulseclient = @("EOF")
autospawn = no
EOF

  file { "/etc/pulse/client.conf":
    content => $pulseclient,
  }

  -> file { "/etc/pulse/client.conf.d/01-enable-autospawn.conf":
    ensure => absent,
  }
  
  -> file { '/etc/pipewire':
    ensure => directory
  }
  
  -> file { '/etc/pipewire/pipewire.conf':
    content => epp("${module_name}/pipewire.conf.epp"), 
  }

  -> file { '/etc/pipewire/jack.conf':
    content => epp("${module_name}/jack.conf.epp"), 
  }

  -> file { '/etc/pipewire/pipewire-pulse.conf':
    content => epp("${module_name}/pipewire-pulse.conf.epp"), 
  }

  -> file { '/etc/pipewire/client-rt.conf':
    content => epp("${module_name}/client-rt.conf.epp"), 
  }

  -> file { '/etc/pipewire/client.conf':
    content => epp("${module_name}/client.conf.epp"), 
  }

  -> file {  '/etc/ld.so.conf.d/pipewire-jack-x86_64-linux-gnu.conf':
    source => 'file:///usr/share/doc/pipewire/examples/ld.so.conf.d/pipewire-jack-x86_64-linux-gnu.conf'
  }


  # -> exec { '/usr/sbin/setcap cap_net_raw,cap_net_admin+ep /usr/bin/pipewire-avb': }
  #-> exec { '/usr/sbin/setcap cap_net_raw,cap_net_admin+ep /usr/bin/pipewire': }
} 
