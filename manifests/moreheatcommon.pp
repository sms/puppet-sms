# cleanup
class sms::moreheatcommon
(
  String $user = 'user',
  Boolean $softwaregl = false, 
)
{

  $rmpkgs = [
    'xfce4',
    'xfce4-power-manager',
    'rpi-connect',
    #'avahi-daemon', used by rtpmidi
    'cups',
    'cups-daemon',
    'modemmanager',
    'networkmanager',
    'tor',

  ]

  ensure_packages($rmpkgs, {ensure => 'absent'})

  $rmpkgs = [
    'avahi-daemon',
  ]

  ensure_packages($rmpkgs, {ensure => 'installed'})


  file_line { 'sudo_rule_nopw':
    path => '/etc/sudoers',
    line => 'user     ALL=(ALL) NOPASSWD:ALL'
  }

  put::userunit { 'mhextra':
    command     => '/home/user/src/More-Heat-Than-Light/run-boot-extra.sh',
    user        => 'user',
    autostart   => true,
    autorestart => true,
    after       => [ 'dbus.service' ],
    wants       => [ 'dbus.service' ],
    partof      => [ 'dbus.service' ],
    wantedby    => [ 'dbus.service' ],
  }



}
