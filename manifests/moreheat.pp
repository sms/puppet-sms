# videoplayer
class sms::moreheat
(
  String $user = 'user',
  String $userdir = '/home/user',
)

{
  require put::debian
  require sms::moreheatcommon

  put::userunit { 'mhplay':
    command     => '/home/user/src/More-Heat-Than-Light/run-boot.sh',
    user        => 'user',
    autostart   => true,
    autorestart => true,
    after       => [ 'xsession.target' ],
    wants       => [ 'xsession.target' ],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }
  put::userunit { 'mhtemp':
    command     => '/home/user/src/More-Heat-Than-Light/run-boot-temp.sh',
    user        => 'user',
    autostart   => true,
    autorestart => true,
    after       => [ 'xsession.service' ],
    wants       => [ 'xsession.service' ],
    partof      => [ 'xsession.service' ],
    wantedby    => [ 'xsession.service' ],
  }




}
