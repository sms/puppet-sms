# cleanup
class sms::supercollider
(
  String $user = 'user',
  Boolean $softwaregl = false, 
)
{

  $pkgs = [
    'supercollider',
  ]

  ensure_packages($pkgs, {ensure => 'present'})

  put::userunit { 'supercollider':
    command     => '/usr/bin/supernova -u 12345 -B 0.0.0.0',
    user        => 'user',
    autostart   => true,
    autorestart => true,
    after       => [ 'pipewire.service' ],
    wants       => [ 'pipewire.service' ],
    partof      => [ 'pipewire.service' ],
    wantedby    => [ 'pipewire.service' ],
  }



}
