class sms::molot (
  String $user = 'user',
  String $userdir = '/home/user',
) {

  $pkgs = [
  'lv2-dev',
  'lv2-c++-tools',
  ]

  ensure_packages($pkgs, {ensure => 'installed'})


  put::buildfromgit { 'molot-lite':
    revision => 'master', # < this works with websocket for sure
    repo      => 'https://github.com/magnetophon/molot-lite.git',
    commands  => [
      'make', 
      'make install -C Molot_Mono_Lite',
      'make install -C Molot_Stereo_Lite'
    ],
  }
}
