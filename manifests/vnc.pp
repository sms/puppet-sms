# vnc server (listens on local network without password)
class sms::vnc
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  include put::xfdesktop

  $kpkgs = [
    'x11vnc'
  ]

  ensure_packages($kpkgs, {ensure => 'installed'})




 put::userunit { 'x11vnc':
    command     => '/usr/bin/x11vnc -nopw -scale 1/2 -rfbport 5905 -shared -forever -loop -localhost',
    environment => ['JACK_NO_AUDIO_RESERVATION=1'],
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'jack.service', 'xsession.target'],
    wants       => [ 'jack.service', 'xsession.target'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }

}

