# sets up jack, and pulseaudio (instead of pipewire)
class sms::jack (
  String  $jack_cmdline = lookup('sms::audio::jack_cmdline', undef, undef, '-d alsa'),
  String $user = 'user',
  String $userdir = '/home/user',
  Boolean $netmaster = lookup('sms::audio::netmaster', undef, undef, false),
  )
  {
  
  require sms::audio

   file { '/etc/pulse/default.pa':
    content => epp("${module_name}/default.pa.epp"), 
  }
  
  Exec {'/usr/bin/apt install -y pulseaudio-module-jack': }

  class {'sms::audioservices':
      pipewire =>  false,
  }


  if $netmaster {
    put::userunit { 'jack_netmaster':
      command => '/usr/bin/jack_load netmaster',
      user    => 'user',
      after   => [ 'jack.service' ],
      wants   => [ 'jack.service' ],
      #partof  => [ 'jack.service'],
      }
  }
  firewall { '991 jack net':
    dport  => '19000',
    proto  => 'udp',
    action => 'accept',
  }


  firewall { '993 pulse':
    dport  => '4317',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '993 pulse udp':
    dport  => '4317',
    proto  => 'udp',
    action => 'accept',
  }
  firewall { '993 pulse 4713':
    dport  => '4713',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '993 pulse udp 4713':
    dport  => '4713',
    proto  => 'udp',
    action => 'accept',
  }


}
