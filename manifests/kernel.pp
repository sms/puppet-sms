# kernel
class sms::kernel (
  )
  {
  tag 'sms'
  require put::debian


  $srcdir = '/root/src'

  $rmpkgs = [
      'linux-headers-6.12.6-rt-amd64',
      'linux-headers-rt-amd64',
      'linux-image-6.12.6-rt-amd64',
      'linux-image-rt-amd64'
  ]
  ensure_packages($rmpkgs, {ensure => 'absent'})



  case $facts['os']['distro']['codename']  {
    'buster': {
      $kversion = "4.19.0-18-rt-amd64"
    }
    'bullseye': {
      #$$kversion = "5.10.0-10-rt-amd64"
      #$kversion = "5.16.0-0.bpo.4-rt-amd64"        
 #     $kversion = "5.10.0-21-rt-amd64"
       $kversion = "5.10.0-26-rt-amd64"
  
  }
    'bookworm': {
      #$$kversion = "5.10.0-10-rt-amd64"
      #$kversion = "5.17.0-1-rt-amd64"
      $kversion = "6.1.0-17-rt-amd64"
 
    }
    'trixie': {
      #$$kversion = "5.10.0-10-rt-amd64"
      # no RT kernel in trixie FIXME
      
      #$kversion = "6.6.11-rt-amd64"
      #$kversion = "6.6.15-rt-amd64"
      #$kversion = "6.5.0-5-amd64"
      #$kversion = "6.10.9-rt-amd64"
      $kversion = "rt-amd64"
    }

    default: {
      fail("Unsupported Debian-flavor machine: ${::lsbdistcodename}")
    }
    
  }

  $kpkgs = [
    "linux-image-${kversion}",
    "linux-headers-${kversion}",
  ]
 
  #FIXME: this way of installing kernel is annoying, all the time have to update the version cause old versions are not in repo anymore
  #$defaultboot = "Debian GNU/Linux, with Linux ${kversion}"
  
  #exec { "/usr/sbin/grub-set-default \'${defaultboot}\'":
  #}


  #FIXME: disabled for now
  #ensure_packages($kpkgs, {ensure => 'installed'})


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  package { 'linux-image-6.10.9-rt-amd64':
    ensure => absent
  }

  package { 'linux-image-5.17.0-rt-amd64':
    ensure => absent
  }

  package { 'linux-image-5.2.0-0.bpo.2-amd64':
    ensure => absent
  }
  package { 'linux-image-4.19.0-11-amd64':
    ensure => absent
  }
  package { 'linux-image-4.19.0-12-amd64':
    ensure => absent
  }
 
  package { 'linux-image-4.19.0-12-rt-amd64':
    ensure => absent
  } 

  package { 'linux-image-5.10.0-12-amd64':
    ensure => absent
  } 

  package { 'linux-image-5.10.0-13-amd64':
    ensure => absent
  } 

  package { 'linux-image-5.10.0-21-amd64':
    ensure => absent
  } 


    $defaultgrub = @("EOF")
GRUB_DEFAULT=saved
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet"
GRUB_CMDLINE_LINUX=""
EOF

  file { "/etc/default/grub":
    content => $defaultgrub,
  }

  -> exec { '/usr/sbin/update-grub':
  }



  #package { 'linux-image-5.2.0-0.bpo.3-amd64':
  #  ensure => absent
  #}
    
}
