# service to sync stuff from nextcloud
class sms::cloud
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{


 put::userunit { 'sync_cloud':
    command     => '/home/user/src/streaming-media-stuff/scripts/sync_cloud.sh',
    user        => 'user',
    autostart   => true,
    autorestart => true,
    after       => [ 'xsession.target'],
    wants       => [ 'xsession.target'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
  }

  file { '/home/user/.config/rclone':
    ensure => directory
  }

  file { '/home/user/.config/rclone/rclone.conf':
    content => epp("${module_name}/rclone.conf.epp"), 
    owner => 'user'
  }


}
