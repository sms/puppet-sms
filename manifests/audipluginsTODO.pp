
  put::buildfromgit { 'voiceoffaust':
    repo      => 'https://github.com/magnetophon/VoiceOfFaust.git',
    revision  => 'master',
    commands  => [
      'make',
      'make install'
    ]
  }
 
  put::buildfromgit { 'DEL2':
    repo      => 'https://github.com/magnetophon/DEL2.git',
    revision  => 'main',
    commands  => [
      '/root/.cargo/bin/cargo build --release',
    ]
  }

  put::buildfromgit { 'mephisto.lv2':
    repo      => 'https://git.open-music-kontrollers.ch/~hp/mephisto.lv2',
    revision  => 'master',
    commands  => [
      'meson build',
      'cd build',
      'ninja -j4',
      'ninja test',
      'sudo ninja install',
    ]
  }


