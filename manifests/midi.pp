# sets up midi
class sms::midi (
  String $user = 'user',
  String $userdir = '/home/user',

  )
  {
 
    require sms::etcdefault
    
     put::userunit { 'qmidinet':
        command       => "/usr/bin/qmidinet -n 3 -j -a off -g",
        user          => 'user',
        autorestart => true,
        after        => [ 'jack.service' ],
        partof      => [ 'xsession.target' ],
        wantedby    => [ 'xsession.target' ],
     
      }

$pkgs = [
    'qmidinet',
    'a2jmidid'
]

  ensure_packages($pkgs, {ensure => 'installed'})

# $aj_snapshot = @(EOF)
# <aj-snapshot>
# <alsa>
# <client name="System">
# <port id="0" />
#    <port id="1" />
#  </client>
#  <client name="Midi Through">
#    <port id="0" />
#
# </client>  <client name="ProjectMix">
#    <port id="0">
#      <connection client="QmidiNet" port="0" />
#
#   </port>    <port id="1" />
#
# </client>  <client name="aj-snapshot" />
#  <client name="QmidiNet">
#
#   <port id="0">      <connection client="ProjectMix" port="0" />
#
#   </port>    <port id="1" />
#    <port id="2" />
#
#
# </client></alsa></aj-snapshot>
#
# EOF
#
# file { '/etc/aj-snapshot.conf':
#    content => $aj_snapshot,
#    notify  => Put::Userunit['aj-snapshot'],
#
# }
#
# put::userunit { 'aj-snapshot':
# command => '/usr/bin/aj-snapshot --alsa -d /etc/aj-snapshot.conf',
#    user    => 'user',
#    autorestart => true,
#    after   => [ 'dbus.service'],
#    wants   => [ 'dbus.service' ],
#    partof  => [ 'xsession.target' ],
#    wantedby    => [ 'xsession.target' ],
#
#
# }
  put::userunit { 'aj-snapshot':
	ensure  => 'absent'
  }
  put::userunit { 'a2jmidid':
	command       => "/usr/bin/a2jmidid -e -u",
	user          => 'user',
	autorestart => true,
	after        => [ 'jack-plumbing.service' ],
	partof      => [ 'xsession.target' ],
	wantedby    => [ 'xsession.target' ],

  }



  }
