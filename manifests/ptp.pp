# time sync
class sms::ptp (
  )
  {

    #
    # FIXME: figure out how this is supposed to work on systems without (hardware or software) timestamping support
    # FIXME: write function that automatically detects ethernet cards (and timestamp options)
    # FIXME: figure out how to automagically have a master
    #
    # https://github.com/jclark/pc-ptp-ntp-guide
    # https://tsn.readthedocs.io/timesync.html
    # https://linuxptp.sourceforge.net/i210-rework/i210-rework.html
    # https://github.com/jclark/pc-ptp-ntp-guide/blob/main/gps.md
    #


  package { 'ntp':
    ensure => absent
    } 

  $pkgs = [
    'linuxptp',
    'chrony',
    'ethtool',

  ]

  ensure_packages($pkgs, { ensure => 'installed'})

  file { '/etc/linuxptp/timemaster.conf':
    content => epp("${module_name}/timemaster.conf.epp"), 
  }

  file { '/etc/apparmor.d/usr.sbin.chronyd':
    content => epp("${module_name}/usr.sbin.chronyd.epp"), 
  }






}
