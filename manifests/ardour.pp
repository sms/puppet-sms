# sms audio
class sms::ardour (
  String $user = 'user',
  String $userdir = '/home/user',
) {
  file { "${userdir}/media/ardour":
    ensure => directory,
    owner  => $user,

  }

  require put::debian
  require sms::audio
  require sms::gitrepos

$pkgs = [
    'qjackctl',
    'ardour',
    'ebumeter',
    'moc',

    #plugins
    'swh-lv2',
    'tap-plugins',
    'fil-plugins',
    'calf-plugins',
    'x42-plugins',
    'ardour-lv2-plugins',
    'eq10q',
    'invada-studio-plugins-lv2',
    'lsp-plugins-lv2',
    'mda-lv2',
    'so-synth-lv2',
    'zam-plugins',
    'qjackctl'
  ]

  ensure_packages($pkgs, {ensure => 'installed'})


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  exec { 'download  openstage':
    #                  https://github.com/jean-emmanuel/open-stage-control/releases/download/v1.26.2/open-stage-control_1.26.2_amd64.deb
      command => "wget https://github.com/jean-emmanuel/open-stage-control/releases/download/v1.26.2/open-stage-control_1.26.2_amd64.deb",
      creates => '/root/open-stage-control_1.26.2_amd64.deb"',
  }
  ->exec { 'install openstage':
      command => "/usr/bin/dpkg -i open-stage-control_1.26.2_amd64.deb",
      creates => '/usr/bin/open-stage-control',
  }

  -> vcsrepo { '/home/user/src/ardour-control':
    ensure   => 'latest',
    provider => git,
    owner    => $user,
    group    => $user,
    remote   => 'origin',
    revision => 'master',
    user     => $user,
    keep_local_changes => false,
    source   => {
        'origin' => 'https://github.com/jean-emmanuel/ardour-control.git',
     },
    submodules => false,
  }

  -> firewall { '121 ardour open stage control':
      dport  => '8080',
      proto  => 'tcp',
      action => 'accept',
    }

  -> put::userunit { 'open-stage-control':
    command     => "/usr/bin/open-stage-control /usr/lib/open-stage-control/resources/app/ -l /home/user/src/ardour-control/ardour.json -c /home/user/src/ardour-control/ardour-plugins-module.js -s 127.0.0.1:3819",
    user        => 'user',
    autostart   => false,
    autorestart => true,
    after       => [ 'ardour.service', 'jack.service' ],
    wants       => [ 'ardour.service', 'jack.service' ],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
    environment => ['ELECTRON_RUN_AS_NODE=1'],

  }

  # disabled for now, somehow not workign
  #   put::userunit { 'ardour':
  #  command     => '/home/user/src/streaming-media-stuff/scripts/ardourfromtemplate.sh',
  #  user        => 'user',
  #  autostart   => false,
  #  autorestart => true,
  #  after       => [ 'jack.service'],
  #  wants       => [ 'jack.service' ],
  #  partof      => [ 'xsession.target' ],
  #  wantedby    => [ 'xsession.target' ],
  #}

  firewall { '121 ardour websocket':
    dport  => '3818',
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '121 ardour osc':
    dport  => '3819',
    proto  => 'tcp',
    action => 'accept',
  }




}

