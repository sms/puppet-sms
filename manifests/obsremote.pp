# extra untested obs plugins etc.
class sms::obsremote
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian
  require sms::audio
  require put::nodejs
  require sms::pythonstuff
  require sms::etcdefault

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'


    include put::nodejs

  if ! $facts['os']['distro']['codename']  == 'trixie' { 
 
    put::buildfromgit { 'obs-tablet-remote':
      repo     => 'https://github.com/t2t2/obs-tablet-remote.git',
      revision => '386d6d865892d21bbb088c80ef01a759d7f89ed7',
      commands => ['npm install', 'npm run build -- --homepage /'],
      require => Put::Buildfromgit['obs-studio'],
    }
  }
  put::buildfromgit { 'obs-web':
    repo     => 'https://github.com/Niek/obs-web.git',
    revision => 'master',
    commands => ['npm i', 'npm run build'],
    require => Put::Buildfromgit['obs-studio'],
  }

  
  put::userunit {'obs-web':
    command => '/usr/bin/npm run dev',
    cwd     => '/usr/local/src/obs-web/',
    user    => 'user',
    after       => [ 'jack.service'],
    partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],

    #wants       => [ 'obs.service'],
    #partof      => [ 'xsession.target' ],
    #wantedby    => [ 'xsession_started.target' ],
  }

  firewall { '121 obs-web':
    dport  => '5000',
    proto  => 'tcp',
    action => 'accept',
  }

    #FIXME: install obs-tablet-remote somewhere, and not run it in development mode all the time
    put::userunit {'obs-tablet-remote':
      command => '/usr/bin/npm run dev',
      cwd     => '/usr/local/src/obs-tablet-remote/',
      user    => 'user',
      after       => [ 'jack.service'],
      partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],
    }

}

