class sms::nvidia {
  require put::debian


  
  $kpkgs = [
    'nvidia-detect',
    'nvidia-driver',
    'firmware-misc-nonfree',
    #'nvidia-xconfig',
    'initramfs-tools',
  ]
  package{ 'dracut':
    ensure => absent,
  }
  ensure_packages($kpkgs, {ensure => 'installed'})

  
  #  $pulseclient = @("EOF")
  #autospawn = no
  #EOF

  #  file { "/etc/pulse/client.conf":
  #    content => $pulseclient,
  #  }

  # MODNAME="module_name"; echo "omit_dracutmodules+=\" $MODNAME \"" >> /etc/dracut.conf.d/omit-$MODNAME.conf

  # kernel param rd.driver.blacklist=module_name


}
