# ffado mixer
class sms::ffado
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{


  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

 $pkgs = [
   'ffado-mixer-qt4'
 ]
 ensure_packages($pkgs, {ensure => 'installed'}) 

 file { '/usr/lib/udev/rules.d/60-ffado.rules':
   ensure                            => absent
 }
  #  $rmpkgs = [
  #    'libffado2',
  #    'jackd2-firewire',
  #    'ffado-dbus-server',
  #    'ffado-mixer-qt4',
  #
  #  ]
  #
   #ensure_packages($rmpkgs, {ensure => 'absent'}) 
  #
  #
  # $pkgs = [
  #   'scons',
  #   'cdbs',
  #   'dh-python',
  #   'libconfig++-dev',
  #   'libconfig-dev',
  #   'libconfig9',
  #   'libdbus-c++-1-0v5',
  #   'libdbus-c++-bin',
  #   'libdbus-c++-dev',
  #   'libecore1',
  #   'libeina1a',
  #   'libglibmm-2.4-dev',
  #   'libiec61883-dev',
  #   'libsigc++-2.0-dev',
  #   'libxml++2.6-dev',
  #   'pyqt5-dev-tools',
  #   'python3-dbus.mainloop.pyqt5',
  #   'python3-pyqt5',
  #   'python3-pyqt5.sip'
  # ]
  #
   # ensure_packages($pkgs, {ensure  => 'installed'}) 


 #  
 # file {'/usr/local/src/ffado':
 #   ensure => 'directory'
 # }
 # -> archive { '/opt/ffado.tar.gz':
 #    ensure        => present,
 #    extract       => true,
 #    extract_path  => '/usr/local/src/ffado',
 #    extract_command => '/usr/bin/tar xfz %s --strip-components=1',
 #    source        => 'http://www.ffado.org/files/libffado-2.4.5.tgz',
 #    creates       => '/usr/local/src/ffado/NEWS',
 #    #cleanup       => true,
 #  }
 #  -> exec {'sed "1c#!/usr/bin/python3" support/tools/ffado-diag > support/tools/ffado-diag':
 #    cwd => '/usr/local/src/ffado'
 #  }
 #  -> exec{'scons':
 #    cwd => '/usr/local/src/ffado'
 #  }
 #  -> exec{'cp src/libffado.so src/libffado.so.2':
 #    cwd => '/usr/local/src/ffado'
 #  }
 #  put::userunit { 'ffado-dbus-server':
 #    command       => "/usr/local/src/ffado/support/dbus/ffado-dbus-server",
 #    environment   => [
 #      'LD_LIBRARY_PATH=/usr/local/src/ffado/src',
 #    ],
 #    user          => 'user',
 #    partof      => [ 'xsession.target' ],
 #    wantedby    => [ 'xsession.target' ], 
 #  }
 #

}
