# shared audio stuff
class sms::talkback (
  String $user = 'user',
  String $userdir = '/home/user',
  )
  {

  $srcdir = '/root/src'


  $pkgs = [
    'jack-tools',
  ]



  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }

  ensure_packages($pkgs, {ensure => 'installed'})


  put::userunit { 'talkback_send_srt':
    command   => '/home/user/src/streaming-media-stuff/scripts/talkback_send_srt.sh',
    user      => 'user',
    autorestart => true,
    after     => [ 'pulseup.service' ],
    wants     => [ 'pulseup.service' ],
    #partof      => [ 'xsession.target' ],
    wantedby    => [ 'xsession.target' ],
 

  }


    
}
