# libndi5
class sms::ndi5
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  include sms::nsm


  firewall { '123 ndi mdns tcp':
    dport  => '5353',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '123 ndi mdns udp':
    dport  => '5353',
    proto  => 'udp',
    action => 'accept',
  }
  firewall { '123 ndi msg tcp':
    dport  => '5960',
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '123 ndi msg udp':
    dport  => '5960',
    proto  => 'udp',
    action => 'accept',
  }

 

  $srcdir = '/usr/local/src'
  $ndi_version = '5.6.1'

  file { "/usr/lib/libndi.so.${ndi_version}":
    ensure => present,
    #owner  => root,
    #group  => root,
    #mode   => '0700',
    source => "puppet:///modules/${module_name}/libndi.so.${ndi_version}",
  }
 # -> file { '/usr/lib/libndi.so.5':
 #   ensure => 'link',
 #   target => "/usr/lib/libndi.so.${ndi_version}",
 # }
  -> file { '/usr/lib/libndi.so':
    ensure => 'link',
    target => "/usr/lib/libndi.so.${ndi_version}",
  }
  -> exec { 'ndilib5 ldconfig':
    command => '/usr/sbin/ldconfig',
  }

}
