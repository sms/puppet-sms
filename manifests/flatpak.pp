# flatpak
class sms::flatpak (
  )
  {
  
  $pkgs = [
    'flatpak',
  ]

  ensure_packages($pkgs, { ensure => 'installed' })


  Exec { '/usr/bin/flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo': }


 
}
