# samba
class sms::samba (
  )
  {
  
  $pkgs = [
    'samba',
  ]

  ensure_packages($pkgs, { ensure => 'installed' })



  file { '/mnt/radio/shared':
    ensure => directory,
    mode   => '0777'
  }

  file { '/etc/samba/smb.conf':
    content => epp("${module_name}/smb.conf.epp"), 
  }

  sms::service { 'samba server':
    dport => 445,
    type => '_smb._tcp',
    proto => 'tcp',
  }


 
}
