# gstd
class sms::gstd
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{

  require sms::pythonstuff

  $kpkgs = [
    'libedit-dev',
    'libdaemon-dev',
    'automake',
    'libtool',
    'pkg-config',
    'libgstreamer1.0-dev',
    'libgstreamer-plugins-base1.0-dev',
    'libglib2.0-dev',
    'libjson-glib-dev',
    'gtk-doc-tools',
    #'libncursesw5-dev',
    'libdaemon-dev',
    'libjansson-dev',
    'libsoup2.4-dev',
    'python3-pip',
    'libedit-dev',
    'ninja-build',
    #'meson',
  ]

  ensure_packages($kpkgs, {ensure => 'installed'})

  put::buildfromgit { 'gst-interpipe':
    repo     => 'https://github.com/RidgeRun/gst-interpipe.git',
    revision => 'master',
    #cwd => 'build',
    commands => [
      'meson --prefix=/usr -Denable-gtk-doc=false build',
      'ninja -C build',
      'ninja -C build install',
    ],
  }
 

  put::buildfromgit { 'gstd':
    repo     => 'https://github.com/RidgeRun/gstd-1.x.git',
    revision => 'master',
    commands => [
      'meson build --prefix=/usr',
      'ninja -C build',
      'ninja -C build install'
    ],
  }
 



}

