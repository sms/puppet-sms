# soundux soundboard
class sms::soundboard
(
  String $user = 'user',
  String $userdir = '/home/user',
  )

{
  tag 'slow', 'building_software', 'sms'
  require put::debian

  Exec {
    user    => 'root',
    cwd     => '/root/',
    timeout => 9999,
    path    => '/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin',
    environment => ['HOME=/root' ]
  }


  $srcdir = '/usr/local/src'

  $pkgs = [
   'build-essential',
  'cmake',
  'libx11-dev',
  'libxi-dev',
  'libwebkit2gtk-4.0-dev',
  'libappindicator3-dev', 
  'libssl-dev', 
  'libpulse-dev',
  'libayatana-appindicator3-dev',
  ]

  ensure_packages($pkgs, {ensure => 'installed'}) 

  put::buildfromgit { 'soundux':
    revision => 'master',
    #srcdir => "/usr/local/src",
    repo      => 'https://github.com/Soundux/Soundux.git',
    cwd       => './build',
    commands  => [
      'cmake ..',
      'cmake build . --config Release',
      'make install',
    ]
  } 

    put::userunit {'soundux':
      command => '/usr/local/bin/soundux',
      cwd     => '/home/user',
      user    => 'user',
      after       => [ 'pulseup.service'],
      partof      => [ 'xsession.target' ],
      wantedby    => [ 'xsession.target' ],
    }

}

